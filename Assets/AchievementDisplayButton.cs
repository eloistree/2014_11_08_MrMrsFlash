﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementDisplayButton : MonoBehaviour
{

    public Image iconRenderer;
    public AchievementDisplayerZone displayZone;

    public Achievement achievement;
    public Sprite iconLocked;
    public Sprite iconUnlocked;

    public void Start()
    {
        SetAchievement(achievement);

    }


    public void SetAchievement(Achievement achievementToSet)
    {


        achievement = achievementToSet;
        bool paramsValide = !(achievementToSet == null || (string.IsNullOrEmpty(achievementToSet.title) && string.IsNullOrEmpty(achievementToSet.description)));
        gameObject.SetActive(paramsValide);

        if (iconRenderer != null && achievement != null)
        {
            if (achievement.isUnlocked)
                iconRenderer.sprite = achievement.spriteIcon != null ? achievement.spriteIcon : iconUnlocked;
            else
                iconRenderer.sprite = iconLocked;
        }
        iconRenderer.color = Color.white;

    }

    public void OnMouseEnter()
    {
            Sound.Play("OverAchievement");
    }
    //public void OnMouseExit()
    //{
    //    if (displayZone == null) return;
    //    displayZone.DisplayDefault();
    //}



    public void FocusOnThisAchievement()
    {
           if (displayZone == null) return;
            displayZone.DisplayAchievement(achievement);

    
    }

    public void Deactivate()
    {

        displayZone.DisplayDefault();
        gameObject.SetActive(false);
    }
}