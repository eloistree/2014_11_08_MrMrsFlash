﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class AchievementDisplayerZone : MonoBehaviour {


    public Text achievementTitle;
    public Text achievementDescription;
    public RectTransform extraParent;
    public GameObject extraContent;
    public float delayToResetStandard=3.5f;
    private float countDownReset;

    public AchievementDisplayButton [] buttons;

    public void Update() {
        if (countDownReset > 0)
        {
            countDownReset -= Time.deltaTime;
            if (countDownReset <= 0)
                DisplayDefault();
        }
    }

    public void DisplayAchievements(Achievement [] achievements, GameObject extraPrefab ) {

        if (extraContent)
        { Destroy(extraContent); }
        if (extraPrefab!=null && extraParent!=null)
        {
            GameObject gamo = Instantiate(extraPrefab, extraParent.position, extraParent.rotation) as GameObject;
            gamo.transform.SetParent( extraParent,false);
            extraContent = gamo;
            
        }

        if (buttons == null ) return;
        int i =0;
        while (i<buttons.Length) 
        {
            if(buttons[i]!=null)
            if (achievements != null
                && i <achievements.Length)
            {

                buttons[i].SetAchievement(achievements[i]);
            }
            else
                buttons[i].Deactivate();

            i++;
        }
    }

    public void DisplayDefault() 
    {
        if (extraContent)
            extraContent.SetActive(true);
        if (achievementTitle != null)
        {
            achievementTitle.gameObject.SetActive(false);
        }
        if (achievementDescription != null)
        {
            achievementDescription.gameObject.SetActive(false);
        }
        countDownReset = 0f ;
    }
  
    public void DisplayAchievement(Achievement achievement) 
    {
         bool display=achievement != null;

        if(extraContent)
            extraContent.SetActive(false);
         
        if (achievementTitle != null) { 
            achievementTitle.text = achievement.title;
            achievementTitle.gameObject.SetActive(true);
        }
        if (achievementDescription != null) { 
            achievementDescription.text = achievement.description;
            achievementDescription.gameObject.SetActive(true);
        }

        countDownReset = delayToResetStandard ;
    }
}
