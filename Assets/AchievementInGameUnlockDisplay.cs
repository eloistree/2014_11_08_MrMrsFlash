﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementInGameUnlockDisplay : MonoBehaviour {


    public Text title;
    public Animator animator;
    public string keyDisplayHF = "AchievementUnlock";


   
    public void OnEnable()
    {
        Achievements.onAchievementUnlocked += DisplayAchievement;
    }

 
    public void OnDisable()
    {

        Achievements.onAchievementUnlocked -= DisplayAchievement;

    }
    private void DisplayAchievement(string idName)
    {
        if(title){
             Achievement achi = Achievements.GetAchievementInfo(idName);
            if(achi!=null)
             title.text = achi.title;
        }
        if (animator && !string.IsNullOrEmpty(keyDisplayHF))
        {
            animator.SetTrigger(keyDisplayHF);
        }
    }


    
}
