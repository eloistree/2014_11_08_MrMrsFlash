﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Achievements : MonoBehaviour {
    public LevelAchievements [] levelsAndAchivements;
    public delegate void OnAchievementUnlocked(string idName);
    public static OnAchievementUnlocked onAchievementUnlocked;
    public static Dictionary<string, Achievement> allAchievementsInGame = new Dictionary<string, Achievement>();
    public static Dictionary<string, GameObject> allExtraContentsInGame = new Dictionary<string, GameObject>();
    public static Dictionary<string, Achievement[]> allLevelAchievementsInGame = new Dictionary<string, Achievement[]>();
    
    public void Awake() {

        foreach(LevelAchievements la in  levelsAndAchivements)
        AddLevel(la);

    }

  
    public static void RandomUnlock()
    {
        Debug.Log("Random Unlock achievement:" + allAchievementsInGame.Count);
        foreach (Achievement achievement in allAchievementsInGame.Values)
            if (achievement != null)
                achievement.isUnlocked = Random.Range(0,2)==0;
    }

    public static void ResetAchievementToLocked()
    {
        Debug.Log("Reset achievement to lock:" + allAchievementsInGame.Count);
        foreach (Achievement achievement in allAchievementsInGame.Values)
            if (achievement!=null)
                achievement.isUnlocked = false;
    }

    public static void AddAchievement(Achievement achievement)
    {
        if (achievement == null || string.IsNullOrEmpty(achievement.idName))
            return;
        if (allAchievementsInGame.ContainsKey(achievement.idName))
            return;
        allAchievementsInGame.Add(achievement.idName, achievement);
    }
    public static void AddLevel(LevelAchievements level)
    {
        if(level==null)return;
        foreach (Achievement a in level.achievements)
            AddAchievement(a);
        AddExtraContent(level.levelName,level.extraContent);
        if (! allLevelAchievementsInGame.ContainsKey(level.levelName))
            allLevelAchievementsInGame.Add(level.levelName, level.achievements);
        

    }

    private static void AddExtraContent(string levelName ,GameObject extraPrefab)
    {
        if (levelName == null || extraPrefab == null) return;
        if (!allExtraContentsInGame.ContainsKey(levelName))
            allExtraContentsInGame.Add(levelName, extraPrefab);
        
    }

    public static Achievement GetAchievementInfo(string idName) 
    {
        Achievement achievement;
        if(allAchievementsInGame.TryGetValue(idName, out achievement))
        return achievement;
        return null;
    }

    public static void SetAchievementUnlockState(string idName, bool unlocked)
    {
        bool isUnlocked = GetAchievementUnlockState(idName);
        //Debug.Log("Unlock Achievement..." );
        if (!isUnlocked && unlocked && onAchievementUnlocked != null)
        {

            onAchievementUnlocked(idName);
            Debug.Log("Achievement " + idName + " : Unlock");
        }
        PlayerPrefs.SetInt(idName, unlocked ? 1 : 0);
    }
    public static bool GetAchievementUnlockState(string idName)
    {
        return PlayerPrefs.GetInt(idName)!=0 ? true : false;
    }

    public static Achievement[] GetLevelAchievements(string idName, out GameObject extraContant)
    {
        Achievement[] valueToReturn;
        allLevelAchievementsInGame.TryGetValue(idName, out valueToReturn);
        allExtraContentsInGame.TryGetValue(idName, out extraContant);
        return valueToReturn;
    }
}

[System.Serializable]
public class LevelAchievements 
{
    public string levelName;
    public Achievement [] achievements;
    public GameObject extraContent;

}

[System.Serializable]
public class Achievement 
{
    public string idName;
    public Sprite spriteIcon;
    public string title;
    public string description;
    public int level;
    public bool isUnlocked
    {
        get { return Achievements.GetAchievementUnlockState(idName); }
        set { Achievements.SetAchievementUnlockState(idName,value); }
    }
    
    
}
