﻿using UnityEngine;
using System.Collections;

public class ActiateObjectOnDistance : MonoBehaviour {


    public ApplyEffectBaseOnDistance applyEffectTo;
    public float pourcentToActivate=0.7f;


    public void OnEnable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect += SetActive;

    }

    public void OnDisable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect -= SetActive;

    }

    private void SetActive(Transform obj, float pourcentToApply)
    {
        if (!obj==null) return;
        if (pourcentToApply > pourcentToActivate && !obj.gameObject.activeSelf)
            obj.gameObject.SetActive(true);
        else if (pourcentToApply <= pourcentToActivate && obj.gameObject.activeSelf)
            obj.gameObject.SetActive(false);

    }
}
