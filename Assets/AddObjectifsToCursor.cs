﻿using UnityEngine;
using System.Collections;

public class AddObjectifsToCursor : MonoBehaviour {


    public string indicatorName = "main";
	// Use this for initialization
	void Start () {
	    QuestIndictor [] indicators = GameObject.FindObjectsOfType<QuestIndictor>() as QuestIndictor[];
        QuestIndictor indi=null;
        foreach(QuestIndictor qi in indicators)
            if(qi!=null && "main".Equals(qi.idName))
            {
            indi=qi;
            }
        if(indi==null )return;
        Objectif [] objectifs = GameObject.FindObjectsOfType<Objectif>() as Objectif [];
        foreach(Objectif obj in objectifs)
        {
            if(obj!=null)
                indi.AddCursor(obj.transform);
        }

	}
	
}
