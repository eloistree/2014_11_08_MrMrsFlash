﻿using UnityEngine;
using System.Collections;

public class AddToMenuAtStart : MonoBehaviour {


    public GravityMenu menu;
    public Transform [] elements;
    public void  Awake(){

        if (menu)
        {
            foreach (Transform elem in elements)
                menu.Add(elem);
        }
	}
	
}
