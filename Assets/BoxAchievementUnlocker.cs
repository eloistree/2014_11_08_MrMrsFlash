﻿using UnityEngine;
using System.Collections;
using Flash.Level3;

public class BoxAchievementUnlocker : MonoBehaviour {

    public string achievementId = "";
    void OnEnable()
    {

        CurrentLevelManager.InstanceInScene.onLevelWin += DoesPlayerUnlockAchievement;
    }


    void OnDisable()
    {
        CurrentLevelManager.InstanceInScene.onLevelWin -= DoesPlayerUnlockAchievement;

    }
    public void DoesPlayerUnlockAchievement(float timeLeft)
    {
        if (MovingBox.TryToMoveBox <= 1) {
            Achievements.SetAchievementUnlockState(achievementId, true);
        }
    }
}
