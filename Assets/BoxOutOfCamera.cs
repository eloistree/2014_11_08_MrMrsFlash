﻿using UnityEngine;
using System.Collections;

public class BoxOutOfCamera : MonoBehaviour {

    public string glassBoxKeySound = "boxFallGlass";
    public string groundBoxKeySound = "boxFallGround";
    public float volumeRatio = 0.5f;

    public bool isOutOfCamera = true;


    public void Awake() 
    { 
        Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
        isOutOfCamera  =  viewPos.x < 0 || viewPos.x > 1 || viewPos.y < 0 || viewPos.y > 1;
    }


    public void Update()
    {
        if (Camera.main == null|| isOutOfCamera) return;
    
      Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
     
      if (viewPos.x < 0 || viewPos.x > 1)
      {
          Sound.Play(glassBoxKeySound, null, false, Sound.SoundPickUpType.Randomly, 2f, 100f*volumeRatio, 0, 50);

          Destroy(this);
      }
      else if (viewPos.y < 0 || viewPos.y > 1) 
      {
          Sound.Play(groundBoxKeySound, null, false, Sound.SoundPickUpType.Randomly, 2f, 100f * volumeRatio, 0, 50);
          Destroy(this);
      }

    }



}
