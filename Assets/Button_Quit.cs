﻿using UnityEngine;
using System.Collections;

public class Button_Quit : MonoBehaviour {


    public void OnMouseDown() 
    {
        Quit();
    }

    public void Quit() {
        Debug.Log("Quit");
        Application.Quit();
    }
}
