﻿using UnityEngine;
using System.Collections;

public class ChangeLightOrderWithTaque : MonoBehaviour {

    public taque taque;
    public HightLight hightlight;
    
    void OnEnable()
    {
        if (taque == null)
            taque = GetComponent<taque>() as taque;
        if (hightlight == null)
            hightlight = GetComponent<HightLight>() as HightLight;
        if (taque)
        {
            taque.onSortingChange += ChangeHightLightSort;
            taque.onTaqueFixed += HightLightOff;
        }
    }
    private void HightLightOff()
    {
        if (hightlight && hightlight.spriteRender)
        {
            hightlight.spriteRender.enabled = false;
            hightlight.enabled = false;
        }
    }

    private void ChangeHightLightSort(SpriteRenderer renderer, int sortLayerNumber)
    {
        if (hightlight && hightlight.spriteRender)
            hightlight.spriteRender.sortingOrder = sortLayerNumber-1;
    }
    void OnDisable()
    {
        if (taque) { 
            taque.onSortingChange -= ChangeHightLightSort;

            taque.onTaqueFixed -= HightLightOff;
        }
    }
}
