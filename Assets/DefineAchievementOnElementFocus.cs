﻿using UnityEngine;
using System.Collections;

public class DefineAchievementOnElementFocus : MonoBehaviour {


    public RelocateElementCenterOf elementFocus;
    public AchievementDisplayerZone achievementDisplayZone;


    public void OnEnable()
    {
        if (elementFocus != null)
            elementFocus.onNewElementFocus += SetAchievementWhenFocus;
    }
    public void OnDisable()
    {
        if (elementFocus != null)
            elementFocus.onNewElementFocus -= SetAchievementWhenFocus;
    }
    private void SetAchievementWhenFocus(Transform oldElementFocus, Transform newElementFocus)
    {
        if(achievementDisplayZone){
            BelongToLevel level = newElementFocus.GetComponent<BelongToLevel>() as BelongToLevel;
            if (level == null) return;
            GameObject extra =null;
            Achievement[] achievements = Achievements.GetLevelAchievements(level.levelName, out extra);
            achievementDisplayZone.DisplayAchievements(achievements, extra);
        }
    }
}
