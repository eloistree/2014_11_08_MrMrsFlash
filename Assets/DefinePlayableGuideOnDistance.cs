﻿using UnityEngine;
using System.Collections;

public class DefinePlayableGuideOnDistance : MonoBehaviour {

    public ApplyEffectBaseOnDistance applyEffectTo;
    public PlaySelectedGame playButton;
    public float pourcentToBeFocus = 0.8f;

    public void OnEnable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect += SetGuide;

    }

    public void OnDisable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect -= SetGuide;

    }

    private void SetGuide(Transform obj, float pourcentToApply)
    {
        if (obj == null || playButton == null) return;
        CreateGuideAndLoad guideLoader = obj.GetComponent<CreateGuideAndLoad>() as CreateGuideAndLoad;
        if (guideLoader == null) return;
        
        if (pourcentToApply < pourcentToBeFocus)
        {

            playButton.SetGameToPlay(null);
        }
        else
        {
            playButton.SetGameToPlay(guideLoader);
        }
    }
}
