﻿using UnityEngine;
using System.Collections;

public class DifficultyToAnimator : MonoBehaviour {

	 public Animator animator;
     public StarsDifficultyDisplay stars;

	void Start () {

        LevelDifficulty levelDifficulty = GameObject.FindObjectOfType<LevelDifficulty>() as LevelDifficulty;
        if (levelDifficulty != null)
        {
            if (animator)
                animator.SetInteger("Difficulty", (int)levelDifficulty.difficultyCount - 1);

            if (stars)
            {
                int idiff = Mathf.Clamp((int)levelDifficulty.difficultyCount - 1, 0, 3);
                stars.SetStarDifficulty(idiff);
            }
        }
       
	
	}

	
}
