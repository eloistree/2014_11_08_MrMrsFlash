using System;
using UnityEditor;
using UnityEngine;

public class BuildScript {
	
	private static string[] allScenes = { "Assets/Scenes/01.unity" };
    public enum PlatformeType { WIN, MAC, LINUX, WEBPLAYER, IPHONE, ANDROID, WINDOWPHONE }
	
	[MenuItem ("Tools/Build/WIN MAC LINUX")] 
	public static void PerformBuildAllWinMacLinux() 
	{
		Debug.Log("Building All desktop versions");
        RunBuild(PlatformeType.WIN);
        RunBuild(PlatformeType.MAC);
        RunBuild(PlatformeType.LINUX);
	}
    [MenuItem("Tools/Build/All")] 
    public static void All()
    {
        Debug.Log("All building");
        RunBuild(BuildScript.PlatformeType.ANDROID);
        RunBuild(BuildScript.PlatformeType.WINDOWPHONE);
        RunBuild(BuildScript.PlatformeType.MAC);
        RunBuild(BuildScript.PlatformeType.WIN);
        RunBuild(BuildScript.PlatformeType.WEBPLAYER);
    }





    private static void RunBuild(BuildScript.PlatformeType type) 
	{
	
		switch (type)
		{
            case BuildScript.PlatformeType.WIN:
				BuildPipeline.BuildPlayer(allScenes, "builds/win/build.exe", BuildTarget.StandaloneWindows, BuildOptions.None);
				break;
            case BuildScript.PlatformeType.MAC:
				BuildPipeline.BuildPlayer(allScenes, "builds/mac/build", BuildTarget.StandaloneOSXUniversal, BuildOptions.None);
				break;
            case BuildScript.PlatformeType.LINUX:
				BuildPipeline.BuildPlayer(allScenes, "builds/linux/build", BuildTarget.StandaloneLinuxUniversal, BuildOptions.None);
				break;
			case BuildScript.PlatformeType.WEBPLAYER:
				BuildPipeline.BuildPlayer(allScenes, "builds/webplayer/build", BuildTarget.WebPlayer, BuildOptions.None);
				break;
     
	
            case BuildScript.PlatformeType.ANDROID:
				BuildPipeline.BuildPlayer(allScenes, "builds/android/build", BuildTarget.Android, BuildOptions.None);
				break;
            case BuildScript.PlatformeType.IPHONE:
				BuildPipeline.BuildPlayer(allScenes, "builds/iphone/build", BuildTarget.iPhone, BuildOptions.None);
				break;
		
		}

		
	}

	

}
