﻿using UnityEngine;
using System.Collections;

public class FocusCircleMenuSound : MonoBehaviour {



    public RelocateElementCenterOf relocate;
    public string keySoundElementFocus = "ElementFocus";
    public string keySoundElementEnterZone = "ElementEnterZone";


    public void OnEnable()
    {
        if (relocate)
            relocate.onEnterFocusZone += PlayEnterZoneSound;
        if (relocate)
            relocate.onNewElementFocus += PlayFocusSound;

    }
    public void OnDisable()
    {
        if (relocate)
            relocate.onEnterFocusZone -= PlayEnterZoneSound;
        if (relocate)
            relocate.onNewElementFocus -= PlayFocusSound;

    }

    private void PlayFocusSound(Transform oldElementFocus, Transform newElementFocus)
    {
        Sound.Play(keySoundElementFocus);
    }

    private void PlayEnterZoneSound(Transform element)
    {
        Sound.Play(keySoundElementEnterZone);
    }
}
