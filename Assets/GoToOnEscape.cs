﻿using UnityEngine;
using System.Collections;

public class GoToOnEscape : MonoBehaviour {

    public string sceneToLoadOnEscape = "MainMenu";
    public GameObject[] activateBeforeLoad;
    public GameObject[] instanciateBeforeLoad;
    public float timeBeforeLoad =1f;

    public void Update() {

        if (Input.GetKeyDown(KeyCode.Escape)&& Application.CanStreamedLevelBeLoaded(sceneToLoadOnEscape)) {

            if (activateBeforeLoad != null)
                foreach (GameObject gamo in activateBeforeLoad)
                    if (gamo) gamo.SetActive(true);
            if (instanciateBeforeLoad != null)
                foreach (GameObject gamo in instanciateBeforeLoad)
                    if (gamo) Instantiate(gamo);
            Invoke("Load", timeBeforeLoad);
        }
    }

    public void Load() {

        Application.LoadLevel(sceneToLoadOnEscape);
    }
	
}
