﻿using UnityEngine;
using System.Collections;

public class HightLight : MonoBehaviour {

    public SpriteRenderer spriteRender;
    public float pourcentChangeBySecond = 1f;
    public float pourcent;
    public bool addPourcent;

    void Start() {
        if (spriteRender == null)
            spriteRender = GetComponent<SpriteRenderer>() as SpriteRenderer;
    
    }

	void Update () {
        if (spriteRender == null) return;

        if (addPourcent)
            pourcent += pourcentChangeBySecond * Time.deltaTime;
        else
            pourcent -= pourcentChangeBySecond * Time.deltaTime;

        if (pourcent > 1f) { addPourcent = false; pourcent = 1f;  }
        else if (pourcent < 0f) { addPourcent = true; pourcent = 0f; }

        Color currentColor = spriteRender.color;
        currentColor.a = pourcent;
        spriteRender.color = currentColor;
	}
}
