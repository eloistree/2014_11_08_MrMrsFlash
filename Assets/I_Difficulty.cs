﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


    public interface I_Difficulty
    {

        void SetDifficulty(GameDifficulty.Type difficulty);
        GameDifficulty.Type GetDifficulty();

    }

        public static class GameDifficulty
        {
            public enum Type : int { Easy = 0, Normal = 1, Hard = 2, FirstTime = -1 }
            public static int GetDifficultyIndex(Type difficulty)
            {
                int index = -1;
                switch (difficulty)
                {
                    case Type.Easy: index = 0; break;
                    case Type.Normal: index = 1; break;
                    case Type.Hard: index = 2; break;
                    case Type.FirstTime: index = -1; break;

                }
                return index;
            }
            public static Type GetDifficultyIndex(int index)
            {
                Type difficulty = 0;
                switch (index)
                {
                    case 0: difficulty=Type.Easy; break;
                    case 1: difficulty = Type.Normal; break;
                    case 2: difficulty = Type.Hard; break;
                    case -1: difficulty = Type.FirstTime; break;

                }
                return difficulty;
            }

        }
