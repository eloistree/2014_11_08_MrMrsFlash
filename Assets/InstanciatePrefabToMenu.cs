﻿using UnityEngine;
using System.Collections;

public class InstanciatePrefabToMenu : MonoBehaviour
{


    public GravityMenu menu;
    public Transform[] elements;
    public MenuThemeItem[] menuItems;
    public void Awake()
    {

        if (menu)
        {
            foreach (Transform elem in elements)
            {
                if (elem)
                {
                    Transform gamo = Transform.Instantiate(elem) as Transform;
                    menu.Add(gamo);
                }
            } 
            foreach (MenuThemeItem elem in menuItems)
            {
                if (elem)
                {
                    Transform gamo = Transform.Instantiate(elem.transform) as Transform;
                    MenuThemeItem item = gamo.GetComponent<MenuThemeItem>() as MenuThemeItem;
                    menu.Add(item);
                    menu.Add(gamo);
                }
            }
        
        } 
         
    }

}
