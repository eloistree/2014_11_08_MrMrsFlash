﻿using UnityEngine;
using System.Collections;

public class LifeToAnimator : MonoBehaviour {

    public Animator animator;

	void Start () {

           PlayerLife playerLife =  GameObject.FindObjectOfType<PlayerLife>() as PlayerLife;
           if (playerLife != null)
           {
               animator.SetInteger("Life", playerLife.life);
           }
	
	}
	
}
