﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuSoundItem : MonoBehaviour {

    public Image itemRenderer;
    public Sprite spriteOn;
    public Sprite spriteOff;
    public bool stateOn = true;
    public bool activateOnMouseDown = false;

    public string audioSoundPrefPlayer = "Flash:GlobalSoundPref";

    public void Start() {

        bool isFirstTime = ! PlayerPrefs.HasKey(audioSoundPrefPlayer);
        if(isFirstTime)
        PlayerPrefs.SetFloat(audioSoundPrefPlayer, 1f);

        float volumeWanted = PlayerPrefs.GetFloat(audioSoundPrefPlayer);
       SetState(!(volumeWanted <= 0f));

    }

    public void OnMouseDown() 
    {
        if (activateOnMouseDown)
            InverseState();
     }

    public void InverseState() 
    {
        SetState(!stateOn);
    }

    public void SetState(bool onOff)
    {
        if (itemRenderer && spriteOn && spriteOff)
        {
            itemRenderer.sprite = onOff ? spriteOn : spriteOff;
        }
        AudioListener.volume = onOff ? 1f : 0f;
        PlayerPrefs.SetFloat(audioSoundPrefPlayer, AudioListener.volume);
        stateOn = onOff;
    }
}
