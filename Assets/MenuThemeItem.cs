﻿using UnityEngine;
using System.Collections;

public class MenuThemeItem : MonoBehaviour ,I_Difficulty{


    public Transform root;
    public SpriteRenderer backGround;
    public SpriteRenderer icon;
    public StarsDifficultyDisplay stars;
    public string thematiqueName = "";


    void Start()
    {
        if (!root) root = this.transform;
        if (!backGround  || !stars)
        {

            Debug.LogWarning("Params not valide", this.gameObject);
            Debug.Break();
            return;
        }

        stars.SetDifficulty(GetDifficulty());
    }

    public Transform GetRoot() { return root; }
    public SpriteRenderer GetBackGround() { return backGround; }
    public SpriteRenderer GetIcon() { return icon ; }


    public void SetDifficulty(GameDifficulty.Type difficulty)
    {
        if (stars)
            stars.SetDifficulty(difficulty);
       // if (!string.IsNullOrEmpty(thematiqueName))
        //    FlashGameGlobalData.SetDifficultyOf(thematiqueName,difficulty);
    }

    public GameDifficulty.Type GetDifficulty()
    {
        return FlashGameGlobalData.GetDifficultyOf(thematiqueName);
    }
}
