﻿using UnityEngine;
using System.Collections;

public class MoveAt : MonoBehaviour {

    public float timeToGo;
    public float startTimeToGo;
    public Vector3 startPosition;
    public Quaternion startRotation;
    public Vector3 startScale;
    public Vector3 wantedScale;
    public Transform goAt;
    public Vector3 goAtPos;
    public Quaternion goAtRot;
    public float delay=0f;

    public void SetMoveAt(float timeToGo, Transform goAt, Vector3 scaleWanted, float delay=0f)
    {
        this.goAt = goAt;
        SetMoveAt(timeToGo, goAt.position, goAt.rotation, scaleWanted);
    }
    public void SetMoveAt(float timeToGo, Vector3 pos, Quaternion rot, Vector3 scaleWanted, float delay = 0f) 
    {
        if (scaleWanted == Vector3.zero) scaleWanted = Vector3.one;
        this.timeToGo = this.startTimeToGo = timeToGo;
        this.goAtPos = pos;
        this.goAtRot = rot;
        this.wantedScale = scaleWanted;
        this.delay = delay;

        startPosition = transform.position;
        startRotation = transform.rotation;
        startScale = transform.localScale;
    }

	void Update () {
        if (delay>0f)
        {
            delay -= Time.deltaTime;
            if (delay > 0f) return;
            
        }
        timeToGo -= Time.deltaTime;
        float pourcent =(startTimeToGo-timeToGo)/startTimeToGo;

        pourcent = Mathf.Clamp(pourcent, 0f, 1f);
       
            transform.position = Vector3.Lerp(startPosition, goAtPos, pourcent);
            transform.rotation = Quaternion.Lerp(startRotation, goAtRot, pourcent);
            transform.localScale = Vector3.Lerp(startScale, wantedScale, pourcent);

            if (timeToGo < 0f && goAt)
                transform.parent = goAt;
            if (timeToGo < 0f) Destroy(this);
	
	}
}
