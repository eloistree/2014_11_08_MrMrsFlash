﻿using UnityEngine;
using System.Collections;

public class OnMouseDown_Activation : MonoBehaviour {


    public GameObject[] activateOnClick;
    public GameObject[] deactivateOnClick;

    public bool activateOnMouseDown = false;
    public string keySoundSwitchAchievement = "keySoundPress";

    public void OnMouseDown()
    {
        if (activateOnMouseDown)
        Switch();
        
    }

    public void Switch()
    {
        Sound.Play(keySoundSwitchAchievement);
        foreach (GameObject gamo in activateOnClick)
            if (gamo) gamo.SetActive(true);
        foreach (GameObject gamo in deactivateOnClick)
            if (gamo) gamo.SetActive(false);
    
    }
}
