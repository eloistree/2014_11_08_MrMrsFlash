﻿using UnityEngine;
using System.Collections;
using System;
using System.Diagnostics;

public class OpenWebLinkOnPress : MonoBehaviour {

    public string webPage = "http://www.google.com";
    public bool activateOnMouseDown = false;
    public string keySoundPress ="PressButtonMenu";
    public void OnMouseDown()
    {
        if (activateOnMouseDown)
        OpenLink();
    }

    public void OpenLink() 
    {
        Sound.Play(keySoundPress);
        Application.OpenURL(webPage);
    }
}
