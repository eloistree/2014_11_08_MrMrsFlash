﻿using UnityEngine;
using System.Collections;

public class PhysicsIgnore : MonoBehaviour {
    public string[] layersToIgnore;

    void Awake()
    {

        foreach (string lti in layersToIgnore)
        {
            if (!string.IsNullOrEmpty(lti))
                Physics2D.IgnoreLayerCollision(gameObject.layer, LayerMask.NameToLayer(lti));
        }

    }
}
