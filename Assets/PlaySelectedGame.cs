﻿using UnityEngine;
using System.Collections;

public class PlaySelectedGame : MonoBehaviour {

    public CreateGuideAndLoad guideToPlay;
    public float pourcentToBeFocus=0.8f;

    public void Awake() {
        SetGameToPlay(guideToPlay,false);
    }

    public void SetGameToPlay(CreateGuideAndLoad guide, bool withCheck=true) 
    {
            if (withCheck &&  guide == guideToPlay) return;
            guideToPlay =guide;
            gameObject.SetActive(guide!=null);
    
    }

    public void Play() {
        if (guideToPlay != null)
        {
            guideToPlay.CreateAndStartGuide();
        }
    }


    
   
}
