﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {

    public string keySound = "";
    public bool onMouseEnter = true;
    public bool onMousePressed = true;

    public float volume = 1f;

    public void OnMouseEnter()
    {
        if (onMouseEnter)
            PlayTheSound();

    }
    public void OnMouseDown()
    {
        if (onMousePressed)
            PlayTheSound();

    }
    public void PlayTheSound() 
    {

        Sound.Play(keySound, null, false, Sound.SoundPickUpType.Randomly,3,volume,0,100);
    
    }
}
