﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestIndictor : MonoBehaviour {

    public string idName=""; 
    public SpriteRenderer questIndicator;
    public List<QuestIndictorCursor> cursors= new List<QuestIndictorCursor>();

    public GameObject indictorPrefab;
    public Transform center;
    public Transform [] initialTargets;

    public bool respectLocalScaleOnIndicator = true;
    public void Awake() 
    {
        foreach (Transform target in initialTargets) {
            AddCursor(target);
        }
    }

    public void AddCursor(Transform target)
    {
        QuestIndictorCursor cursor = GetCursor(target);
        if (cursor != null || indictorPrefab == null) return;
        if(null== indictorPrefab.GetComponent<QuestIndictorCursor>() as QuestIndictorCursor)
        {
            Debug.Log("Cursor prefab do not contain QuestIndicatorCursor scrit !",this.gameObject);
            return;
        }
        Transform c = this.center==null? transform:this.center;
        GameObject newCursor = Instantiate(indictorPrefab,c.position,c.rotation) as GameObject;
        newCursor.transform.parent = c;
        if (respectLocalScaleOnIndicator) {
            Vector3 newScale = newCursor.transform.localScale;
            newScale.x *= transform.localScale.x;
            newScale.y *= transform.localScale.y;
            newScale.z *= transform.localScale.z;
            newCursor.transform.localScale = newScale;
        }
        cursor = newCursor.GetComponent<QuestIndictorCursor>() as QuestIndictorCursor;
        cursor.target = target;
        cursor.questCursorCenter = c;

        cursors.Add(cursor);
        cursor.SetTarget(target);

    }

    public bool RemoveCursor(Transform target) {
        
        QuestIndictorCursor cursor = GetCursor(target);
        if (cursor) 
        {
           return cursors.Remove(cursor);
        }
        return true;
    }

    public QuestIndictorCursor GetCursor(Transform target)
    {
        foreach (QuestIndictorCursor cursor in cursors) 
        {
            if (cursor.target == target) return cursor;
        }
        return null;
    }

	
}
