﻿using UnityEngine;
using System.Collections;

public class QuestIndictorCursor : MonoBehaviour {


    public SpriteRenderer questCursor;
    public Transform questCursorCenter;
    public Transform target;
    public bool withUpdate=true;

    public void Awake()
    {
        SetTarget(target);
    }

    public void Update()
    {
        if (!withUpdate) return;
        if (target != null)
            SetTarget(target);
      

    }


    public void SetTarget(Transform target)
    {
        this.target = target;
        if (target == null && questCursor != null)
            questCursor.gameObject.SetActive(false);

        if (target != null)
            SetCursorPosition(target.position);

    }
    private void SetCursorPosition(Vector3 pos)
    {
        pos.z = transform.position.z;
        if (questCursor == null) return;
        questCursor.gameObject.SetActive(true);
        transform.LookAt(pos);

    }
}
