﻿using UnityEngine;
using System.Collections;

public class QuickRotate : MonoBehaviour {

    public float rotationSpeed = 60f;
    public Vector3 direction = Vector3.forward;
    public bool random=true;
    public float range=30f;

    void Start() {

        if (Random.Range(0, 2) == 0)
            rotationSpeed = -rotationSpeed;
    
    }

	void Update () {

        float randomValue = random?Random.Range(-range,range):0f;
        transform.Rotate(direction * (rotationSpeed+randomValue )* Time.deltaTime );

	
	}
}
