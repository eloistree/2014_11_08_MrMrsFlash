﻿using UnityEngine;
using System.Collections;
using Flash.Level3;

public class ReceiveBox : MonoBehaviour {


    public Animator animator;
    public string keyWorkReveiceBox = "ReceiveBox";
    public Transform boxHolderPosition;
    public Transform playerRoot;
    public Transform leaveAt;
    public float timeToReceive=0.25f;
    public Vector3 boxScale = Vector3.one*0.5f;


    public void OnTriggerEnter2D(Collider2D col) 
    {
        MovingBox moveBox = col.gameObject.GetComponent<MovingBox>() as MovingBox;
        if (!moveBox) return;
        if (moveBox.type == MovingBox.BoxType.Good)
        {
            moveBox.IsInGoodHand = true;
            if (col.gameObject.rigidbody2D)
                col.gameObject.rigidbody2D.isKinematic = true;

            if (col.gameObject.collider2D != null)
                col.gameObject.collider2D.enabled = false;

            if(moveBox)
            moveBox.enabled = false;
            if (boxHolderPosition != null)
            {
                MoveAt moveAt = col.gameObject.AddComponent<MoveAt>() as MoveAt;
                moveAt.SetMoveAt(timeToReceive, boxHolderPosition, boxScale);
            }
            if (playerRoot != null &&leaveAt!=null) {
                MoveAt moveAt = playerRoot.gameObject.AddComponent<MoveAt>() as MoveAt;
                //Vector3 goPos = playerRoot.position;
                //goPos.x *= 10f;
                moveAt.SetMoveAt(3f, leaveAt.position, playerRoot.rotation, playerRoot.localScale, timeToReceive);
            }

            BoxOutOfCamera boxOut = col.gameObject.GetComponent<BoxOutOfCamera>() as BoxOutOfCamera;
            if (boxOut)
                Destroy(boxOut);

            Sound.Play("ReceiveGoodBox");
            if(animator!=null)
            animator.SetTrigger(keyWorkReveiceBox);
        }
    }
}
