﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RelocateElementCenterOf : MonoBehaviour {


    public CirclePositioningElements circleElements;
    public TurnAround turnTool;
    public Transform landmarkPoint;
    public List<Transform> elements;
    public float pourcentAwayByUnity = 1f;
    public float powerRatio = 1f;
    public float reduceRatioByToFocus = 0.5f;
    public float pourcentSensibility = 0.1f;
    public bool onlyIfNotPressing=true;
    private bool outZone;

    public delegate void OnNewElementFocus(Transform oldElementFocus, Transform newElementFocus);
    public OnNewElementFocus onNewElementFocus;
    public delegate void OnEnterFocusZone(Transform element);
    public OnEnterFocusZone onEnterFocusZone;

   private Transform lastFocus;
	void Update () {

        if (circleElements == null || turnTool == null || landmarkPoint==null) return;
        if (elements == null || elements.Count<1) return;
        if (onlyIfNotPressing && Input.GetMouseButton(0)){
            outZone = true;
            return;
        }
        Transform objReferent = circleElements.circleCenter;
        Transform closer = GetCloserPoint(landmarkPoint,elements);
        

        float distance = GetLateralDistanceOfCenterPosition(closer.position, landmarkPoint);
        float pourcent = Mathf.Clamp(distance*pourcentAwayByUnity,-1f,1f);
        if ( Mathf.Abs(pourcent) > Mathf.Abs(pourcentSensibility) || pourcent <= -Mathf.Abs(pourcentSensibility))
        {
            turnTool.AddTroqueForce(pourcent*pourcent*(pourcent>0f?1f:-1f) * powerRatio*Time.deltaTime);
            outZone = true;
            if (onEnterFocusZone != null)
                onEnterFocusZone(closer);
        }
        else if (outZone)
        {
            turnTool.ReduceTorqueByRatio(reduceRatioByToFocus);
            outZone = false;
        }


        if (closer != lastFocus)
        {
            if (onNewElementFocus != null)
                onNewElementFocus(lastFocus, closer);
            lastFocus = closer;
        }
	}

    public void Add(Transform obj)
    {
        if (elements != null)
            elements.Add(obj);
    }

    public float GetLateralDistanceOfCenterPosition(Vector3 position, Transform refObject)
    {
        Vector3 rootPosition = refObject.position;
        Quaternion rootRotation = refObject.rotation;
        Vector3 relocatedPoint = Utility.BasedOnPosition.Relocated(position, rootPosition, rootRotation, false, true, false);
        return relocatedPoint.x;
    }
    private Transform GetCloserPoint(Transform landmarkPoint, List<Transform> elements)
    {
        Transform result = null;
        float distance = float.MaxValue;
        foreach (Transform elem in elements)
        {
            float distPtP = Vector3.Distance(elem.position, landmarkPoint.position);
            if (distPtP < distance)
            {
                result = elem;
                distance = distPtP;
            }
        }
        return result;
    }
   
}
