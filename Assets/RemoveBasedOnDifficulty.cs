﻿using UnityEngine;
using System.Collections;

public class RemoveBasedOnDifficulty : MonoBehaviour {

    public MonoBehaviour[] scripts;
    public GameObject[] objects;
    public LevelDifficulty.Difficulty[] destroyInDifficulty;
   

    public void Awake() 
    {
        if (LevelDifficulty.InstanceInScene == null) return;
        LevelDifficulty.Difficulty  diff =  LevelDifficulty.InstanceInScene.CurrentDifficulty;
        foreach (LevelDifficulty.Difficulty d in destroyInDifficulty) 
        {
            if (d == diff)
            {

                foreach (MonoBehaviour script in scripts)
                    Destroy(script);
                foreach (GameObject obj in objects)
                    Destroy(obj);
                break;
            }
        }

    }
 
}
