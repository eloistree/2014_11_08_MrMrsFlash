﻿using UnityEngine;
using System.Collections;

public class RunAchievementUnlock : MonoBehaviour {

	
    public string achievementId = "";
    public float timeToRunToUnlock=5f;
    void OnEnable()
    {

        CurrentLevelManager.InstanceInScene.onLevelLost += DoesPlayerUnlockAchievement;
    }


    void OnDisable()
    {
        CurrentLevelManager.InstanceInScene.onLevelLost -= DoesPlayerUnlockAchievement;

    }
    public void DoesPlayerUnlockAchievement(float timeLeft)
    {
        MoveHead moveHead = GameObject.FindObjectOfType<MoveHead>() as MoveHead;
        if(moveHead==null)return;
        if (moveHead.TimeRan > timeToRunToUnlock) {
            Achievements.SetAchievementUnlockState(achievementId, true);
        }
    }

}
