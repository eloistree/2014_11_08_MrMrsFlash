﻿using UnityEngine;
using System.Collections;

public class SetDifficultyOnClick : MonoBehaviour {

    public MenuThemeItem menu;
    public GameDifficulty.Type difficulty;


    public void OnMouseDown() {
        if (menu)
            menu.SetDifficulty(difficulty);
    }
}
