﻿using UnityEngine;
using System.Collections;

public class SoundToPlay : MonoBehaviour {


    public GameObject soundHurlement;
    public GameObject soundTissu;

    public void PlayHurlementVictoire() {
        if (soundHurlement)
            soundHurlement.SetActive(true);
    }
    public void PlayTissuDechirer()
    {
        if (soundTissu)
            soundTissu.SetActive(true);
    
    }
}
