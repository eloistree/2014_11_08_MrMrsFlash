﻿using UnityEngine;
using System.Collections;

public class StabiliserTimeScale : MonoBehaviour {

    private float timeScaleAtLoad;
	void Awake () {
        timeScaleAtLoad = Time.timeScale;
        Time.timeScale=1f;
	}

    void OnDestroy() {
        Debug.Log("Reset timescale to: " + timeScaleAtLoad);
        Time.timeScale = timeScaleAtLoad;
    }
}
