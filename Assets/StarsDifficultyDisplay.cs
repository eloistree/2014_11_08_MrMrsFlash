﻿using UnityEngine;
using System.Collections;

public class StarsDifficultyDisplay : MonoBehaviour , I_Difficulty {


    public Color enableColor = Color.yellow;
    public Color disableColor = Color.grey;
    public Color lockColor = Color.grey;
    public SpriteRenderer[] starDifficulty;
    public GameDifficulty.Type difficulty;


   

   
    public void SetStarDifficulty(int difficultyCount)
    {
        if(starDifficulty==null || starDifficulty.Length<1)return;
        int diffIndex = difficultyCount;
        int  maxLengh =starDifficulty.Length-1;
        if (diffIndex > maxLengh) diffIndex = maxLengh;
            RefreshStarColorDisplay( diffIndex);
    }

    private void RefreshStarColorDisplay(int diffIndex)
    {
        if (starDifficulty == null) return;
        for (int i = 0; i < starDifficulty.Length; i++)
        {
            if(starDifficulty[i]!=null){
                if(i<=diffIndex) starDifficulty[i].color= enableColor;
                else starDifficulty[i].color = disableColor;
            }
        }



    }

    public void SetDifficulty(GameDifficulty.Type difficulty)
    {

        int index = GameDifficulty.GetDifficultyIndex(difficulty);
        this.difficulty = difficulty;
        SetStarDifficulty(index);
    }

    public GameDifficulty.Type GetDifficulty()
    {
        return this.difficulty; 
    }
}
