﻿using UnityEngine;
using System.Collections;

public class TeleportPlayerHere : MonoBehaviour {

    public bool withX = true;
    public bool withY = false;
    public bool withZ = false;
    public bool lookAtRight=true;
	void Start () {

        GameObject player = GameObject.FindWithTag("Player");
        if (player != null)
        {
            Vector3 newPos = player.transform.position;
            if (withX)
                newPos.x = transform.position.x;
            if (withY)
                newPos.y = transform.position.y;
            if (withZ)
                newPos.z = transform.position.z;
            player.transform.position = newPos;


            Vector3 scale = player.transform.localScale;
            scale.x = Mathf.Abs(scale.x) * (lookAtRight?1f:-1f);
            player.transform.localScale = scale;
          
        }
        Destroy(this);
	}
	
}
