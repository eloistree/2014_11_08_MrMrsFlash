﻿using UnityEngine;
using System.Collections;

public class WalkStepSound : MonoBehaviour {

    public MoveHead moveHead;
    public float timeBetween = 0.25f;
    public float countDown;
    private bool isWalking;
    public string idStep = "OneStep";
    private GameObject lastSound;

    public void Start() {

        StartCoroutine(StepSound(timeBetween));
        
    }
    public void OnEnable()
    {
        if (CurrentLevelManager.InstanceInScene != null)
        {
            CurrentLevelManager.InstanceInScene.onLevelEnd += AutoDestroy;
        }
    }

   
    public void OnDisable()
    {
        if (CurrentLevelManager.InstanceInScene != null)
        {

            CurrentLevelManager.InstanceInScene.onLevelEnd -= AutoDestroy;
        }
    }
    private void AutoDestroy(float timeLeft)
    {
        Destroy(this);
    }

    private IEnumerator StepSound(float timeBetween)
    {
        while (true) {
            if (isWalking) { 
                if(lastSound)
                     DestroyImmediate(lastSound);
                lastSound = Sound.Play(idStep);
            }
            yield return new WaitForSeconds(timeBetween);
        }
    }
    public void Update() 
    {
        if (!moveHead) return;
       
        if (isWalking && ! moveHead.isWalking ) 
        {
            isWalking = false;
            countDown = 0f;
        }
        else if (!isWalking && moveHead.isWalking)
        {
            isWalking = true;
            //lastSound = Sound.Play(idStep);
            countDown = 0f;
        }


    
    }
}
