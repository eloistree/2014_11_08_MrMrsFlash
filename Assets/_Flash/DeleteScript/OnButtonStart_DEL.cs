﻿using UnityEngine;
using System.Collections;

public class OnButtonStart_DEL : MonoBehaviour {

    public void StartSequence() 
    {
        LevelGuide.LoadFirstLevel();
    }

    public void StopPlaying() 
    {
        LevelGuide.LoadGameOver();
    }
}
