﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public GameObject Player1;
	public GameObject Player2;
	public bool GameStarted;
	public int CountDownToStart = 5;
	public GameObject CountDown1;
	public GameObject CountDown2;
	public int NumberHitToWin = 3;
	private int Player1Score = 0;
	private int Player2Score = 0;

	public static GameManager Instance { get; private set; }

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		StartCoroutine ("CountDown", CountDownToStart);
	}

	public void isHit(GameObject hitObject)
	{
		if (hitObject == Player1)
		{
			Debug.Log ("Player 1 a été touché");
			Player2Score++;
			if(Player2Score >= NumberHitToWin)
			{
				GameStarted = false;
				CountDown1.SetActive(true);
				CountDown1.GetComponent<Text>().text = "Perdu!";
				CountDown2.SetActive(true);
				CountDown2.GetComponent<Text>().text = "Gagné!";
			}

		}
		if (hitObject == Player2)
		{
			Debug.Log ("Player 2 a été touché");
			Player1Score++;
			if(Player1Score >= NumberHitToWin)
			{
				GameStarted = false;
				CountDown1.SetActive(true);
				CountDown1.GetComponent<Text>().text = "Gagné!";
				CountDown2.SetActive(true);
				CountDown2.GetComponent<Text>().text = "Perdu!";
			}
		}
	}

	IEnumerator CountDown(int cd)
	{
		while(cd > 0)
		{
			CountDown1.GetComponent<Text>().text = cd.ToString();
			CountDown2.GetComponent<Text>().text = cd.ToString();
			yield return new WaitForSeconds(1);
			cd--;
		}
		CountDown1.GetComponent<Text>().text = "GO!";
		CountDown2.GetComponent<Text>().text = "GO!";
		yield return new WaitForSeconds(1);
		GameStarted = true;
		CountDown1.SetActive(false);
		CountDown2.SetActive(false);
	}
}