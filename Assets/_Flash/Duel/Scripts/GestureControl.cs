﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GestureControl : MonoBehaviour {

	private Vector3 screenPoint;
	private Vector3 offset;
	public float MaxZ = 40f;
	public float MinZ = 40f;
	
	void OnMouseDown()
	{
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}

	void OnMouseDrag()
	{
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;

		transform.position = new Vector3(transform.position.x,transform.position.y,curPosition.z);

		if(curPosition.z > 40f)
			transform.position = new Vector3(transform.position.x,transform.position.y,MaxZ);
		if(curPosition.z < -40f)
			transform.position = new Vector3(transform.position.x,transform.position.y,MinZ);
	}
}