﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Launch : MonoBehaviour {
	public GameObject[] LaunchableObjects;
	public float LaunchForce = 100f;
	List<GameObject> LaunchedObjects;

	public static Launch Instance { get; private set; }

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		LaunchedObjects = new List<GameObject> ();
	}

	public void LaunchObject(Vector3 from, Vector3 to, int indexOfLaunchableObject = 0)
	{
		float LaunchForceRand = Random.Range (50f, LaunchForce);
		LaunchedObjects.Add ((GameObject)Instantiate (LaunchableObjects [indexOfLaunchableObject], from, Quaternion.identity));
		LaunchedObjects.Last<GameObject> ().rigidbody.AddForce (to * LaunchForceRand,ForceMode.Impulse);
		LaunchedObjects.Last<GameObject> ().rigidbody.AddTorque (0, 3, 0, ForceMode.Impulse);
	}
}
