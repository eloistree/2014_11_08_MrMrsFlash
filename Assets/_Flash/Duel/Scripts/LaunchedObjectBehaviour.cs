﻿using UnityEngine;
using System.Collections;

public class LaunchedObjectBehaviour : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "LaunchLimit")
			Destroy (gameObject);
		if (other.tag == "Player1" || other.tag == "Player2")
		{
			if(GameManager.Instance.GameStarted)
				GameManager.Instance.isHit(other.gameObject);
			Destroy (gameObject);
		}
			
	}
	
}