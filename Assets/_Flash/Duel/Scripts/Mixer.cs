﻿using UnityEngine;
using System.Collections;

public class Mixer : MonoBehaviour {

	public float LaunchRate = 1f;
	private bool Launchable = true;
	
	void Update () {
		if(Launchable && GameManager.Instance.GameStarted)
		{
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			Launch.Instance.LaunchObject(transform.position, new Vector3(Random.insideUnitCircle.x,0,Random.insideUnitCircle.y),Random.Range(0,Launch.Instance.LaunchableObjects.Length));
			StartCoroutine("DelayedLaunch",LaunchRate);
			LaunchRate = LaunchRate - 0.01f;
		}
	}

	IEnumerator DelayedLaunch(float delay)
	{
		Launchable = false;
		yield return new WaitForSeconds (delay);
		Launchable = true;
	}
}
