﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class AddInitialForce : MonoBehaviour {

    public Vector3 direction = Vector3.up;
    public float force = 10f;

    void OnEnable() 
    {
        rigidbody.AddForce(direction*force,ForceMode.Impulse);
    }
	
}
