﻿using UnityEngine;
using System.Collections;

public class LevelGuide : MonoBehaviour {

    public static LevelGuide InstanceInScene;
    public LevelSequence levelSequence;

    public Transition start;
    public Transition gameover;
    public Transition victory;
    public Transition difficultyChange;
    public Transition defeatLevel;
    public Transition successLevel;
  

    public string [] autoDestructionScene ;
    public int levelCursor;

    public string loadingScene;
    public bool debug=true;

    #region TEMP VAR
    private string lastLevel; 
    #endregion
    #region DELEGATE
    public delegate void OnStartGame(LevelGuide origine,string scene);
    public OnStartGame onStartGame;
    public delegate void OnNextLevel(LevelGuide origine, string oldScene, string newScene);
    public OnNextLevel onNextLevelLoaded;
    public delegate void OnEndOfSequence(LevelGuide origine);
    public OnEndOfSequence onEndOfSequence;
    public delegate void OnGameOver(LevelGuide origine);
    public OnGameOver onGameOver;

    public void NotifyGameStart(string scene)
    {
        if (onStartGame != null)
            onStartGame(this, scene);
    }
    public void NotifyGameOver(string scene)
    {
        if (onGameOver != null)
            onGameOver(this);
    }
    #endregion
    #region INIT
    void Init()
    {
        InstanceInScene = this;
    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        Init();
    }
    public void OnLevelWasLoaded(int i)
    {
        Init();
        string currentSceneName = Application.loadedLevelName;
        //Debug.Log(currentSceneName + "><" + loadingScene);
        if (currentSceneName.Equals(loadingScene))
        loadingScene = "";
       foreach(string sceneName in autoDestructionScene)
           if (!string.IsNullOrEmpty(sceneName) && sceneName.Equals(currentSceneName))
           {
               Destroy(this.gameObject);
               return;
           }

    }

   
    
    #endregion






    #region LOAD LEVEL ZONE







    public static bool LoadLevelWithTransition(LevelGuide lvlGuide, string nextLevelName, string transitionSceneName, float timeLoadMainScene = 0, float timeLoadTransitionScene = 0f)
    {
        if (lvlGuide.IsLoading())
        {
            Debug.Log("Cancel: Already loading.");
            return false;
        }
        //Debug.Log("Load level: " + nextLevelName);
        
        if (!lvlGuide.LoadLevel(nextLevelName, timeLoadMainScene)) return false;
        if (timeLoadMainScene > 0f)
        lvlGuide.LoadLevel(transitionSceneName,timeLoadTransitionScene);

        //Debug.Log("SetNextLevel t(" + timeLoadTransitionScene + "->" + timeLoadMainScene + "): " + nextLevelName);
        lvlGuide.loadingScene = nextLevelName;
        return true;
    }

    private bool IsLoading()
    {
        return ! string.IsNullOrEmpty(loadingScene);
    }
    private bool LoadLevel(string nextLevelName, float timeBetween = 0f)
    {
        if(string.IsNullOrEmpty(nextLevelName) || !Application.CanStreamedLevelBeLoaded(nextLevelName))return false;
        StartCoroutine( LoadLevelWithCoroutine(nextLevelName, timeBetween));
        return true;
    }
    private IEnumerator LoadLevelWithCoroutine(string nextLevelName, float timeBetween=0f)
    {
            if(timeBetween>0f)
            yield return new WaitForSeconds(timeBetween);
            if(Application.CanStreamedLevelBeLoaded(nextLevelName))
            Application.LoadLevel(nextLevelName);
    }
    

    
  
    

    #endregion


    #region LoadSpecific
    public static bool LoadFirstLevel(float timeBeforeLoad ,float timeBeforeLoadTransition=0)
    {
        if (InstanceInScene == null) return false;
        LevelGuide guide = InstanceInScene;
        string nextLevel = guide.start.finalSceneNameToLoad;
        string nextLevelTransition = guide.start.transitionSceneName;
        bool loaded = LoadLevelWithTransition(guide, nextLevel, nextLevelTransition, timeBeforeLoad,timeBeforeLoadTransition);
        if (loaded)
            guide.NotifyGameStart(nextLevel);
        return loaded;
    }
    public static bool LoadGameOver(float timeBeforeLoad ,float timeBeforeLoadTransition=0)
    {
        if (InstanceInScene == null) return false;
        LevelGuide guide = InstanceInScene;
        string nextLevel = guide.gameover.finalSceneNameToLoad;
        string nextLevelTransition = guide.gameover.transitionSceneName;
        bool loaded = LoadLevelWithTransition(guide, nextLevel, nextLevelTransition, timeBeforeLoad,timeBeforeLoadTransition);
        if (loaded)
            guide.NotifyGameOver(nextLevel);
        return loaded;
    }
    public static bool LoadNextLevel(float timeBeforeLoad ,float timeBeforeLoadTransition=0, bool hasLostLife=false)
    {
        if (InstanceInScene == null) return false;
        LevelGuide guide = InstanceInScene;
        bool endOfTheSequence = false; int index=0;
        string nextLevel = guide.levelSequence.GetNextLevel(out endOfTheSequence, out index);
       
        string nextLevelTransition =  guide.successLevel.transitionSceneName;
        if (endOfTheSequence)
        {
            nextLevelTransition = guide.difficultyChange.transitionSceneName;
            timeBeforeLoad = guide.difficultyChange.GetLoadTime_NextScene();
            timeBeforeLoadTransition = guide.difficultyChange.GetLoadTime_TransitionScene();
        }
        if (hasLostLife) nextLevelTransition = guide.defeatLevel.transitionSceneName;

        bool loaded = LoadLevelWithTransition(guide, nextLevel, nextLevelTransition, timeBeforeLoad, timeBeforeLoadTransition);
     
        if (loaded && endOfTheSequence && guide.onEndOfSequence!=null)
        {
            //Debug.Log("Diff++");
            guide.onEndOfSequence(guide);
             
        }
        return loaded;
    }


    public static bool LoadNextLevelWithDeath(float timeBeforeLoad ,float timeBeforeLoadTransition=0f)
    {

        return LoadNextLevel(timeBeforeLoad,timeBeforeLoadTransition, true);
    }
     public static bool LoadNextLevelWithDeath()
    {
        if (InstanceInScene == null) return false;
        LevelGuide guide = InstanceInScene;

        return LoadNextLevel( guide.defeatLevel.GetLoadTime_NextScene(),guide.defeatLevel.GetLoadTime_TransitionScene(),true);
     
     }
	#endregion
    public static bool LoadFirstLevel()
     {
         if (InstanceInScene == null) return false;
         LevelGuide guide = InstanceInScene;
         return LoadFirstLevel(guide.start.GetLoadTime_NextScene(), guide.start.GetLoadTime_TransitionScene());
     }
    public static bool LoadGameOver()
    {
        if (InstanceInScene == null) return false;
        LevelGuide guide = InstanceInScene;
        return LoadGameOver(guide.gameover.GetLoadTime_NextScene(), guide.gameover.GetLoadTime_TransitionScene());
    }
    public static bool LoadNextLevel()
    {
        if (InstanceInScene == null) return false;
        LevelGuide guide = InstanceInScene;
        return LoadNextLevel(guide.successLevel.GetLoadTime_NextScene(), guide.successLevel.GetLoadTime_TransitionScene()); 
    }
   


    public string GetIdThematique()
    {
        if (levelSequence)
            return levelSequence.idThematique;
        return null;
    }


    [System.Serializable]
    public class Transition
    {
        public string finalSceneNameToLoad = "";
        public string transitionSceneName = "";
        public float startAfter=0f;
        public float duration=2f;

        public float GetLoadTime_NextScene()
        {
            return startAfter + duration;
        }

        public float GetLoadTime_TransitionScene()
        {
            return startAfter;
        }
    }

}


