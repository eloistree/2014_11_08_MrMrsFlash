﻿using UnityEngine;
using System.Collections;

public class ChangeDifficyltyOnSequenceEnd : MonoBehaviour {


    public LevelGuide levelGuide;
    public LevelDifficulty difficulty;


    public void OnEnable()
    {

        if (levelGuide == null) levelGuide = transform.GetComponent<LevelGuide>() as LevelGuide;
        if (levelGuide == null) levelGuide = GameObject.FindObjectOfType<LevelGuide>() as LevelGuide;

        if (difficulty == null) difficulty = transform.GetComponent<LevelDifficulty>() as LevelDifficulty;
        if (difficulty == null) difficulty = GameObject.FindObjectOfType<LevelDifficulty>() as LevelDifficulty;

        if (!levelGuide || !difficulty) {
            Debug.Log("No Level or difficulty in the scene. AutoScriptDestruction.", this.gameObject);
            Destroy(this);
            return;
        }

        levelGuide.onEndOfSequence += Increase;
        
        
    
    }

    private void Increase(LevelGuide origine)
    {
        LevelDifficulty.Difficulty before = difficulty.CurrentDifficulty;
        difficulty.Increase();
        LevelDifficulty.Difficulty now = difficulty.CurrentDifficulty;
        Debug.Log("diff ++ :"+now);
        if(levelGuide!=null)
        {
            string idThematique = levelGuide.GetIdThematique();
            if (! string.IsNullOrEmpty(idThematique))
            {
                LevelDifficulty.Difficulty upTo = LevelDifficulty.Difficulty.Easy;
                if (before == LevelDifficulty.Difficulty.Easy && now == LevelDifficulty.Difficulty.Normal)
                    upTo = LevelDifficulty.Difficulty.Easy;
                else if (before == LevelDifficulty.Difficulty.Normal && now == LevelDifficulty.Difficulty.Hard)
                    upTo = LevelDifficulty.Difficulty.Normal;
                else if (before == LevelDifficulty.Difficulty.Hard && now == LevelDifficulty.Difficulty.Hard)
                    upTo = LevelDifficulty.Difficulty.Hard;

                FlashGameGlobalData.SetDifficultyOf(idThematique, ConvertDifficulty(upTo));
               
            }


        }

    }
    public GameDifficulty.Type ConvertDifficulty(LevelDifficulty.Difficulty difficulty)
    {
        switch(difficulty)
        {

            case LevelDifficulty.Difficulty.Normal:
                return GameDifficulty.Type.Normal;
            case LevelDifficulty.Difficulty.Hard:
                return GameDifficulty.Type.Hard;
        }
        return GameDifficulty.Type.Easy;
    }

    public void OnDisable() 
    {
        if(levelGuide)
            levelGuide.onEndOfSequence -= Increase;
    }
}
