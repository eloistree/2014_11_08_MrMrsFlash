﻿using UnityEngine;
using System.Collections;

public class LevelDifficulty : MonoBehaviour
{


    public static LevelDifficulty InstanceInScene;

    public LevelGuide levelGuide;
    public enum Difficulty :int { Easy=0, Normal=1, Hard=2 }
    public Difficulty startDifficulty = Difficulty.Easy;
    private Difficulty _difficulty;
    public int difficultyCount;
    public bool withTimeScaleModification=true;
    public float reduceByDifficulty=0.15f;
	public Difficulty CurrentDifficulty
	{
		get { return _difficulty;}
        set
        {
            if (value < 0) value = 0;
            if (value > Difficulty.Hard) value = Difficulty.Hard;
            if (_difficulty != value && onDifficultyChanged != null)
                onDifficultyChanged(_difficulty,value);
            _difficulty = value;
        }
	}
	
    public delegate void OnDifficultyChanged(Difficulty oldDiff, Difficulty newDiff);
    public OnDifficultyChanged onDifficultyChanged;

    public void Awake()
    {
        CurrentDifficulty = startDifficulty;
    }
    public void Start()
    {
        InstanceInScene = this;
     
    }


    internal void Increase()
    {
        CurrentDifficulty++; difficultyCount++;
        SetTimeScale();
    }

    private void SetTimeScale()
    {
        if (withTimeScaleModification) { 
        Time.timeScale = 1f + (float)difficultyCount * reduceByDifficulty;
        Debug.Log("Timescale:" + Time.timeScale);
        }
    }

    internal void Decrease()
    {

        CurrentDifficulty--; difficultyCount--;
    }
}
