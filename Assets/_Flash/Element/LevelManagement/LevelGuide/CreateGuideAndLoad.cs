﻿using UnityEngine;
using System.Collections;

public class CreateGuideAndLoad : MonoBehaviour {

    public GameObject guidePrefab;
    private LevelGuide levelGuide;
    public bool createOnClick;
    public float maxMagnitudeMoveToClick = 10f;
    public bool startWhenCreate=true;
	// Use this for initialization
	void OnEnable () {
        if (!guidePrefab )
        {
            Debug.LogWarning("No guide is define in the prefab given", this.gameObject);
            Destroy(this);
            return;
        }
        levelGuide = guidePrefab.GetComponent<LevelGuide>() as LevelGuide;
        if ( !levelGuide)
        {
            Debug.LogWarning("The prefab does not have a level guide", this.gameObject);
            Destroy(this);
            return;
        }
        
	}

    private Vector3 startPressing;
    public void OnMouseDown() {
        startPressing = Input.mousePosition;
    }

    public void OnMouseUp() {

        if ((Input.mousePosition - startPressing).magnitude < maxMagnitudeMoveToClick)
        CreateAndStartGuide();

    }

    public void CreateAndStartGuide() 
    {

        if (!guidePrefab) return;

        GameObject gamo = GameObject.Instantiate(guidePrefab) as GameObject;
        levelGuide = gamo.GetComponent<LevelGuide>() as LevelGuide;
        if (!levelGuide) return;
        LevelGuide.InstanceInScene = levelGuide;
        LevelGuide.LoadFirstLevel();
        

    }
	
}
