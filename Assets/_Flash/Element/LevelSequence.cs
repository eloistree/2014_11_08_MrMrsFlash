﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSequence : MonoBehaviour
{
        public string idThematique;
        public List<string> levels;

        #region NextLevel GETTER

        public string GetNextLevel()
        {
            bool atEndOfLoop; int newIndexCursor;
            return GetNextLevel(out atEndOfLoop, out newIndexCursor);
        }
        public string GetNextLevel(out bool atEndOfLoop, out int newIndexCursor)
        {
            string current = GetCurrentLevel();
            return GetNextLevelOf(current, out atEndOfLoop, out newIndexCursor);
        }
        public string GetNextLevelOf(string currentLevel, out bool atEndOfLoop, out int newIndexCursor)
        {
            int indexOfCurrent = GetIndexOf(currentLevel);
            return GetNextLevelOf(indexOfCurrent, out  atEndOfLoop, out newIndexCursor);


        }
        public string GetNextLevelOf(int currentLevel, out bool atEndOfLoop, out int newIndexCursor)
        {
            atEndOfLoop = false; newIndexCursor = -1;
            if (currentLevel < 0) return null;
            if (levels.Count <= 0) { Debug.LogWarning("No level are define !!!"); return null; }
            newIndexCursor = GetNextIndex(currentLevel, out atEndOfLoop);
            string nextLevel = levels[newIndexCursor];
            return nextLevel;
        }
        private int GetNextIndex(int currentLevel, out bool atEndOfLoop)
        {
            atEndOfLoop = false;
            if (levels == null || levels.Count < 1) return 0;
            int i = (currentLevel + 1) % levels.Count;
            atEndOfLoop = i <= currentLevel;

            return i;
        } 
        #endregion
        #region Level GETTER
        private int GetIndexOf(string levelName)
        {
            return levels.IndexOf(levelName);
        }
        public string GetCurrentLevel()
        {
            return Application.loadedLevelName;
        }

        public string GetFirstLevel()
        {
            if (levels.Count < 1) return null;
            else return levels[0];
        }
        public string GetLastLevel()
        {
            if (levels.Count < 1) return null;
            else return levels[levels.Count - 1];
        } 
        #endregion
    }

