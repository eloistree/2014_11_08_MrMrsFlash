﻿using UnityEngine;
using System.Collections;

public class PlayerLife : MonoBehaviour
{
    public int life = 3;
    public int startLife = 3;

    public void Awake() 
    {
        Reset();
    }

    public void OnEnable()
    {
        ListenToCurrentLevel(true);
    }
    public void OnLevelWasLoaded(int level)
    { 

        ListenToCurrentLevel(true);
    }
    public void OnDisable()
    {
        ListenToCurrentLevel(false);
    }

    private void ListenToCurrentLevel(bool onOff)
    {
        if(CurrentLevelManager.InstanceInScene)
            if(onOff)
                CurrentLevelManager.InstanceInScene.onLevelLost += DecreaseLife;
            else 
                CurrentLevelManager.InstanceInScene.onLevelLost -= DecreaseLife;

    }

    private void DecreaseLife(float timeLeft)
    {
        if (life > 0)
        {
            life--;
            if (life <= 0)
            {
                bool gameOver = LevelGuide.LoadGameOver();
                if (!gameOver)
                    Debug.LogWarning("Game Over not activated", this.gameObject);
            }
            else {
                LevelGuide.LoadNextLevelWithDeath();
            }
        }
    }



    public void Reset()
    {
        life = startLife;
    }
}
