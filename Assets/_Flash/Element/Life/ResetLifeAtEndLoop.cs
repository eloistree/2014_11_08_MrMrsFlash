﻿using UnityEngine;
using System.Collections;

public class ResetLifeAtEndLoop : MonoBehaviour {

    public LevelGuide levelGuide;
    public PlayerLife life;


    public void Awake() {
        if (levelGuide == null || life == null) { Destroy(this); return; }
    }
    public void OnEnable()
    {
        ListenToReset(true);
    }
    public void OnLevelWasLoaded(int level)
    {
        ListenToReset(true);
    }
    public void OnDisable()
    {
        ListenToReset(false);
    }

    private void ListenToReset(bool onOff)
    {
        if (levelGuide)
            if (onOff)
                levelGuide.onStartGame+=ResetLife;
            else
                levelGuide.onStartGame -= ResetLife;

    }

    private void ResetLife(LevelGuide origine, string sceneName)
    {
        life.Reset();
    }

}
