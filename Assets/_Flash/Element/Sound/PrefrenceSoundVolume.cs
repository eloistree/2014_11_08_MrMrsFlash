﻿using UnityEngine;
using System.Collections;

public class PrefrenceSoundVolume : MonoBehaviour {

	public AudioSource audiosource;
	public string prefSaveKeyword= "MusicPreference";
	// Use this for initialization
	void Start () {
	
		if (audiosource != null) {
			float sound = PlayerPrefs.GetFloat(prefSaveKeyword);
			audiosource.volume= sound;
		}
		Destroy (this);

	}
}
