﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeTextBasedOnTimer : MonoBehaviour {

    public Text text;
    public Image pourcentDoneImage;


    public void OnEnable()
    {
        LevelTimer.onSecondPast += SecondPast;
    }
    public void OnDisable()
    {
        LevelTimer.onSecondPast -= SecondPast;

    }

    public void SecondPast(int second) 
    {
        if (text == null) return;
        text.text =""+ second;
    }

    public void Update() 
    {
        if (!pourcentDoneImage) return;
        pourcentDoneImage.fillAmount=
        LevelTimer.InstanceInScene.GetPourcentLeftTime();
    }
}
