﻿using UnityEngine;
using System.Collections;

public class LevelTimer : MonoBehaviour {



    private static LevelTimer _instanceInScene;

    public static LevelTimer InstanceInScene
    {
        get { return _instanceInScene; }
        set { _instanceInScene = value; }
    }
    
    
    public static float defaultTime = 9f;
    
    public float timeLeft;
    public float timeDefined;
    public bool custumTime;
    public float levelTime=9f;
    public bool pause;
    public delegate void OnTimerEnd(float definedTime);
    public static OnTimerEnd onTimerEnd;

    public delegate void OnSecondPast(int second);
    public static OnSecondPast onSecondPast;
    private bool timeOut;

   
    void Start()
    {
        //onSecondPast += s =>
        //{
        //    Debug.Log("T:" + s);
        //};

        //onTimerEnd += t =>
        //{
        //    Debug.Log("Time Out (" +t+")" );
        //};

        Init();
    }
    void OnLevelWasLoaded()
    {
        Init();
	}

    public float GetTimeLevel() 
    {
        return custumTime ? levelTime : defaultTime;
    }

    private void Init()
    {
        timeOut = false;
        float t = GetTimeLevel();
        timeDefined = t;
        timeLeft = t;
        InstanceInScene = this;
    }
    private int lastSecond=-1;

	void Update () {
        if (timeOut|| pause) return;
        int nextSecond =(int)(timeLeft - Time.deltaTime);
        if (lastSecond!=nextSecond) {
            lastSecond = nextSecond;
            NotifyNewSecond(nextSecond);
        }
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0) 
        {
            timeLeft = 0f;
            timeOut = true;
            NotifyEndOfTime();
            return;
        }
	}

    private void NotifyNewSecond(int nextSecond)
    {
        if (onSecondPast == null) return;
            onSecondPast(nextSecond);
    }

    private void NotifyEndOfTime()
    {
        if (onTimerEnd == null) return;
        onTimerEnd(timeDefined);

    }

    void OnDestroy() 
    {
        onTimerEnd = null;
    }

    public bool IsTimeOut() {
        return timeOut;
    }

    public float GetPourcentLeftTime() 
    {
        if (timeLeft < 0f) return 0f;
        if (timeLeft > timeDefined) return 1f;
        if (timeDefined <= 0) {
            Debug.LogWarning("Time should not be less then zero !");
            return 1f;
        }
        return timeLeft/timeDefined;
    }

    internal float GetTimeLeft()
    {
        return timeLeft;
    }

    public void SetPause(bool onOff)
    {
        pause = onOff;

    }
}
