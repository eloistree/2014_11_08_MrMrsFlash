﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ApplyEffectBaseOnDistance : MonoBehaviour {

    public Transform landmarkPoint;
    public List<Transform> objsToApply;
    public float minDistance=1f, maxDistance=2f;

    public delegate void ApplyTransformEffect(Transform obj, float pourcentToApply);
    public ApplyTransformEffect updateApplyEffect;

    public void Start() 
    {
       /// updateApplyEffect += DisplayTest;
    }

    public void Add(Transform obj)
    {
        if(objsToApply!=null)
       objsToApply.Add(obj);
    }



    //private void DisplayTest(Transform obj, float pourcentToApply)
    //{
    //    if(pourcentToApply>0)
    //    print(pourcentToApply);
    //}
	
	void Update () {

        if (updateApplyEffect != null)
        {
            CheckValidityAndAutoCorrect();
            foreach (Transform obj in objsToApply)
            {
                float pourcent = GetPourcentInGoodRange(obj);
                updateApplyEffect(obj,pourcent);
            }
        }
        
	
	}

    private float GetPourcentInGoodRange(Transform obj)
    {
        
        float distObjToLandmark = Vector3.Distance(obj.position, landmarkPoint.position) ;
        if (distObjToLandmark < minDistance) return 1f;
        else if (distObjToLandmark > maxDistance) return 0f;
        else return 1f-((distObjToLandmark - minDistance)/(maxDistance-minDistance));
    }

    void CheckValidityAndAutoCorrect()
    {
        if (minDistance > maxDistance) 
        {
            float tmp = minDistance;
            minDistance = maxDistance;
            maxDistance = tmp;
        }
    
    }
}
