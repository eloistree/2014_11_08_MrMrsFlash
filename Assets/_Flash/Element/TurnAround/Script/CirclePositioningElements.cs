﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CirclePositioningElements : MonoBehaviour {

    public Transform circleCenter;
    public float distance = 2f;
    public List<Transform> elements = new List<Transform>();

    public bool repositionAtStart;

    public void Start()
    {
        if (repositionAtStart)
            RepositioningAll();
    }


    public void Reset() 
    {
        elements.Clear();
    }
    public void Add(Transform element, bool linkTo) 
    {
   

        if (element) { 
            elements.Add(element);
            RepositioningAll();
        }
        if (linkTo && circleCenter)
            element.parent = circleCenter;
    }

    private const float rad = Mathf.PI / 180f;
    public void RepositioningAll() 
    {
        int elementNumber = elements.Count;
        float angle = 360f / (float)elementNumber;
        Vector3 forwardDirection = circleCenter.forward;
        Vector3 rightDirection = circleCenter.right;
        int i = 0; float x, y;
        foreach (Transform elem in elements)
        {
            float alpha = angle * (float)i * rad;
            x = Mathf.Sin(alpha);
            y = Mathf.Cos(alpha);

            elem.position = circleCenter.position;
            elem.position+=(forwardDirection * (x * distance));
            elem.position+=(rightDirection * (y * distance));

            i++;
        }
    }

}
