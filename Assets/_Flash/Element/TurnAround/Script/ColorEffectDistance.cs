﻿using UnityEngine;
using System.Collections;

public class ColorEffectDistance : MonoBehaviour {


    public ApplyEffectBaseOnDistance applyEffectTo;
    public Color farColor ;
    public Color nearColor;


    public void OnEnable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect += SetColor;

    }

    public void OnDisable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect -= SetColor;

    }

    private void SetColor(Transform obj, float pourcentToApply)
    {
       Color color =  Color.Lerp(farColor, nearColor, pourcentToApply);
       if (obj != null && obj.renderer != null && obj.renderer.material != null)
           obj.renderer.material.color = color;
    }
}
