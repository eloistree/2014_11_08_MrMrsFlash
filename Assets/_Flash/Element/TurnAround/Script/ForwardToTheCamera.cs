﻿using UnityEngine;
using System.Collections;

public class ForwardToTheCamera : MonoBehaviour {

    public bool inverse;
	
	void Update () {
        if (Camera.main) 
        {
            transform.forward = Camera.main.transform.forward;
            if (inverse) transform.forward *= -1;
        }
	
	}
}
