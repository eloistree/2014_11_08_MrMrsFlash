﻿using UnityEngine;
using System.Collections;

public class GravityMenu : MonoBehaviour {

    public Transform Center
    {
        get { return circlePositionScript.circleCenter; }
    }

    public ApplyEffectBaseOnDistance applyEffectScriptOnRoot;
    public ApplyEffectBaseOnDistance applyEffectScriptOnIcon;
    public ApplyEffectBaseOnDistance applyEffectScriptOnDifficultyStar;
    public CirclePositioningElements circlePositionScript;
    public RelocateElementCenterOf relocateElement;

    public ApplyEffectBaseOnDistance [] otherListeners;

    public void Add(Transform obj) 
    {

        if (obj==null) return;

        foreach (ApplyEffectBaseOnDistance apply in otherListeners)
        {
            if (apply != null)
            {
                apply.Add(obj);
            }
        }
    }

    public void Add(MenuThemeItem obj)
    {

        if (!obj) return;
        SpriteRenderer target;
        target = obj.GetBackGround();
        if (applyEffectScriptOnRoot && target) applyEffectScriptOnRoot.Add(target.transform);
        target = obj.GetIcon();
        if (applyEffectScriptOnIcon && target) applyEffectScriptOnIcon.Add(target.transform);
        StarsDifficultyDisplay star = obj.stars;
        if (applyEffectScriptOnDifficultyStar && star) applyEffectScriptOnDifficultyStar.Add(star.transform);

        if (circlePositionScript) circlePositionScript.Add(obj.transform, true);
        if (relocateElement) relocateElement.Add(obj.GetRoot());

       
    }
}

