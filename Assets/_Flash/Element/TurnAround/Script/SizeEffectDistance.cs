﻿using UnityEngine;
using System.Collections;

public class SizeEffectDistance : MonoBehaviour {


    public ApplyEffectBaseOnDistance applyEffectTo;
    public float minSize=1;
    public float maxSize=1.5f;


    public void OnEnable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect += SizeTheObject;

    }

    public void OnDisable()
    {
        if (applyEffectTo)
            applyEffectTo.updateApplyEffect -= SizeTheObject;

    }

    private void SizeTheObject(Transform obj, float pourcentToApply)
    {
        float size = minSize + pourcentToApply * (maxSize - minSize);
        obj.localScale = Vector3.one * size;
    }
}
