﻿using UnityEngine;
using System.Collections;

public class TurnAround : MonoBehaviour {


    public Vector3 startPoint;
    public float force=10f;
    public ForceMode forceMode;
    public bool inverse;

    void Start() 
    {
        startPoint = transform.position;
    
    }
	void Update () {

        AddFoceWithArrow();
        AddFoceWithMouseDown();
        transform.position = startPoint;
	}

    public bool pressing;
    private Vector3 startPressing;
    private Vector3 lastPressing;
    private Vector3 currentPressing;
    private Vector3 endPressing;
    public float forceByMouseDistance = 1f;
    public bool horizontalAxe;
    private void AddFoceWithMouseDown()
    {
        MousePression();

        if (pressing) 
        {
            Vector3 dir = currentPressing - lastPressing;
            float value = horizontalAxe ? dir.x : dir.y;
            AddTroqueForce(dir.magnitude * (value > 0 ? 1f : -1f) * forceByMouseDistance * (inverse ? 1f : -1f));
        }
    }

    private void MousePression()
    {
        if (Input.GetMouseButtonDown(0))
        {
            endPressing = Vector3.zero;
            lastPressing = currentPressing =startPressing = Input.mousePosition;
            pressing = true;
        }
        if (startPressing != Vector3.zero && Input.GetMouseButton(0))
        {
            lastPressing = currentPressing;
            currentPressing = Input.mousePosition;

        }
        if (startPressing != Vector3.zero && Input.GetMouseButtonUp(0))
        {
            endPressing = Input.mousePosition;
            pressing = false;
        }
    }


    private void AddFoceWithArrow()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            AddTroqueForce( inverse? 1:-1);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {

            AddTroqueForce( inverse ? -1 : 1);
        }
    }

    public void AddTroqueForce( float power) 
    {
        rigidbody.AddTorque(transform.up * force * power, forceMode);
    }
    public void ReduceTorqueByRatio(float pourcentRatio) 
    {
        pourcentRatio = Mathf.Clamp(pourcentRatio, 0f, 1f);
        rigidbody.angularVelocity *= 1 - pourcentRatio;
    }

}
