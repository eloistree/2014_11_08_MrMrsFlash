﻿using UnityEngine;
using System.Collections;

public class FlashGameGlobalData : MonoBehaviour {

    private static string difficultyTag = "Flash:GameDifficulty:";

    public static GameDifficulty.Type GetDifficultyOf(string thematiqueName)
    {
        GameDifficulty.Type difficulty = GameDifficulty.Type.FirstTime;
       
        int i = PlayerPrefs.GetInt(difficultyTag+ thematiqueName,-1);
        difficulty = GameDifficulty.GetDifficultyIndex(i);
        //Debug.Log("Get:" + thematiqueName + "->" + difficulty);
        return difficulty;
    }
    public static void SetDifficultyOf(string thematiqueName,GameDifficulty.Type difficulty,bool onlyIfMore=true)
    {
       // Debug.Log("Set:"+thematiqueName + "->" + difficulty);
        GameDifficulty.Type current = GetDifficultyOf(thematiqueName);
        if (onlyIfMore && difficulty <= current) return;
        PlayerPrefs.SetInt(difficultyTag + thematiqueName, GameDifficulty.GetDifficultyIndex(difficulty));

        //Debug.Log("Set definitif:" + thematiqueName + "->" + difficulty);
    }

    public static void ResetAllData()
    {
        PlayerPrefs.DeleteAll();
    }
}
