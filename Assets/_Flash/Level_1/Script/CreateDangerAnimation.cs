﻿using UnityEngine;
using System.Collections;

public class CreateDangerAnimation : MonoBehaviour {

    public bool playSound;

    public SoundToPlay[] sounds;

    [System.Serializable]
    public class SoundToPlay {
        public float soundDelay;
        public string id;
    }

    public GameObject prefabAnimation;
    public bool actif = false;


    public void Update() {
        if (!playSound) return;
        foreach(SoundToPlay son in sounds)
        {
            if (son.soundDelay >= 0f)
            {
                son.soundDelay -= Time.deltaTime;
                if (son.soundDelay < 0f)
                    Sound.Play(son.id);
            
            }
        }
        
    }

    public void OnTriggerEnter(Collider col)
    {
        playSound = true;
        if (!actif) return;
        // Debug.Log("Danger active: " + descriptionName);
       
        if (prefabAnimation)
            GameObject.Instantiate( prefabAnimation, Vector3.zero, new Quaternion());

    }
}
