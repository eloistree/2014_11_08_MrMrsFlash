﻿using UnityEngine;
using System.Collections;

public class CreateDangerHere : MonoBehaviour {

    void OnEnable() 
    {
        GameObject gamoToCreate = Dangers.GetRandomDanger();
        if(gamoToCreate==null)return;
        GameObject gamo = GameObject.Instantiate(gamoToCreate, transform.position, transform.rotation) as GameObject;
        gamo.transform.parent = this.transform;
    
    }
}
