﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dangers : MonoBehaviour {


    public static Dangers InstanceInScene;

    public GameObject [] dangerPrefabs;
    private List<GameObject> nextToCome= new List<GameObject>();

    void OnEnable() {
        InstanceInScene = this;
    }

    public static GameObject GetRandomDanger()
    {
        if (!InstanceInScene) return null;
        return InstanceInScene.GetNext();
    }

    public static GameObject CreateDangerTo(Vector3 position){
    
      GameObject gamoToCreate = Dangers.GetRandomDanger();
        if(gamoToCreate==null)return null;
        GameObject gamo = GameObject.Instantiate(gamoToCreate, position, gamoToCreate.transform.rotation) as GameObject;
        return gamo;
    }

    private void CheckComingList()
    {
        if (nextToCome.Count < 1)
            nextToCome.AddRange(dangerPrefabs);
    }
    private GameObject GetNext() 
    {
        if (nextToCome.Count < 1)
        { CheckComingList(); }
        int i = Random.Range(0, InstanceInScene.nextToCome.Count);
        GameObject g = nextToCome[i];
        nextToCome.RemoveAt(i);
        return g;
        
    }
}
