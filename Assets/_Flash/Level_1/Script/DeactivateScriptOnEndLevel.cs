﻿using UnityEngine;
using System.Collections;

public class DeactivateScriptOnEndLevel : MonoBehaviour {

    public MonoBehaviour [] scripts;


    public void OnEnable()
    {CurrentLevelManager.InstanceInScene.onLevelEnd += Freeze;}

  
    public void OnDisable()
    {CurrentLevelManager.InstanceInScene.onLevelEnd -= Freeze;}

    private void Freeze(float timeLeft)
    {
        foreach (MonoBehaviour mono in scripts)
            mono.enabled = false;
    }

}
