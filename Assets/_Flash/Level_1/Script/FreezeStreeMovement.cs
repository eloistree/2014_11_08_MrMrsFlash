﻿using UnityEngine;
using System.Collections;

public class FreezeStreeMovement : MonoBehaviour {


    public MoveHead moveHead;
    public void OnEnable()
    {

        CurrentLevelManager.InstanceInScene.onLevelEnd += Freeze;


    }

    private void Freeze(float timeLeft)
    {
        FreezeMovement(true);
    }
    public void OnDisable()
    {

        CurrentLevelManager.InstanceInScene.onLevelEnd -= Freeze;



    }

    public void FreezeMovement(bool onOff) {
        if(moveHead)
        moveHead.freeze = onOff;
    }
}
