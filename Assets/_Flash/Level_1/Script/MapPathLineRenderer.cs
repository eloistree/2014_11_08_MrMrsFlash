﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapPathLineRenderer : MonoBehaviour {

    public MapPathMaker pathMaker;
    public LineRenderer line;
    public Transform start;



    void OnEnable()
    {
        Display(false);
        CheckAndAutoComplete();
        if (!pathMaker) return;
        pathMaker.onPathChanged += DrawPathLine;
    }

    private void CheckAndAutoComplete()
    {
        if (!pathMaker)
        {
            pathMaker = GetComponent<MapPathMaker>() as MapPathMaker;
        }
        if (!line)
        {
            line = GetComponent<LineRenderer>() as LineRenderer;
        }
    }
    void OnDisable()
    {
        Display(false);
        if (!pathMaker) return;
        pathMaker.onPathChanged -= DrawPathLine;
    }

    private void DrawPathLine(ref Path path)
    {
        bool display=true;
        if(path==null)display=false;
        if (path.NoWayPoints()) display = false;

        Display(display);
        if (!display) return;

        int lineNumberPoint=path.points.Count;
        line.SetVertexCount(path.points.Count);
        List<WayPoint> wps = path.points;

        if (start != null && lineNumberPoint > 1) 
        {line.SetPosition(0 + 1, start.position);}

        for (int i=1; i <wps.Count;i++)
        {
                WayPoint wp = wps[i];   
                if(wp!=null)
                line.SetPosition(i, wp.transform.position);            
       
        }


    }
    public void Display(bool onOff)
    {
        if (line) line.enabled = onOff;
    }

    public void Update() 
    {
        if (start!=null && line != null)
            line.SetPosition(0, start.position);
    }
}
