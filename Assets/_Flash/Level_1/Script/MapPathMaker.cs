﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapPathMaker : MonoBehaviour {

    public Transform mapCursor;
    public Path path = new Path();
    public LayerMask layers;

    public delegate void OnPathChanged(ref Path path);
    public OnPathChanged onPathChanged;

    //public bool resetOnClick=true;
    //public bool resetOnClickNextLine = false;
    //public bool resetOnRelease = false;
    public bool freezeInput = true;

    public Transform player;
    public Transform shop;


    public void Update() 
    {
        if (freezeInput) return;

       
        if ( Input.GetMouseButtonDown(0))
                ResetPath(ScreenMouseRay());
        //else if (resetOnClickNextLine && Input.GetMouseButtonDown(0))
        //{
        //    WayPoint wp = ScreenMouseRay();
       
        //    if (path.HasPoint())
        //    {
        //        if (wp != path.PathEnd && path.PathEnd.CanAccesTo(wp))
        //        {
        //            ResetPath();
        //        }
        //    }
        //    else 
        //    {
        //             WayPoint playerCloserWp =player? WayPoint.GetCloser(player.transform.position):null;
        //             if (playerCloserWp && playerCloserWp == wp)
        //                 ResetPath();
        //    }

        //}
        //else
        if (Input.GetMouseButton(0)) 
        {
           WayPoint wp =  ScreenMouseRay();

            if (wp)
            {
                bool add = path.AddPointToPath(wp);
                bool truncate = false; bool addAsNeighbor = false;
                if (!add)
                {
                    truncate = path.TruncatePathTo(wp);
                }
                if (!add && !truncate)
                {
                    addAsNeighbor = path.AddAsNeighbor(wp);
                }
                if (add || truncate || addAsNeighbor) NotifyChange();
            }
        }

        //if (Input.GetMouseButtonUp(0) && resetOnRelease)
        //{
        //    ResetPath();
        //}
    }

    void OnDisable()
    {

        path.points.Clear();
        NotifyChange();
    }

    private void ResetPath(WayPoint wp) {
        path.Reset();
        if (player != null)
        {
            WayPoint playerCloserWp = WayPoint.GetCloser(player.transform.position);
            path.AddPointToPath(playerCloserWp);
        }
        else path.AddPointToPath(wp);
        NotifyChange();
    }

    public WayPoint ScreenMouseRay()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = Mathf.Infinity;
        WayPoint wp =null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if ( Physics.Raycast(ray, out hit,200f, layers))
        {
            Vector3  pos = hit.point;
           

            if (mapCursor) 
            {
                mapCursor.position = pos;
            }
            wp= WayPoint.GetCloser(pos);
           
        }
        return wp;


    }

    public void NotifyChange()
    {
        if (onPathChanged != null)
            onPathChanged(ref path);
    }


    public Transform GetNextWayPointPosition()
    {
        if (path == null || path.NoWayPoints()) return null;
        return path.PathStart.transform ;
    }

    public void RemoveFirstWayPoint()
    {
        path.RemoveFirst();
        NotifyChange();
    }

    public WayPoint GetNextWayPoint()
    {

        if (path == null || path.points.Count <= 1) return null;
        return path.points[1];
    }

    internal WayPoint GetFirstWayPoint()
    {
        if (path == null || path.points.Count < 1) return null;
        return path.points[0];
    }
}

[System.Serializable]
public class Path 
{
    public List<WayPoint> points = new List<WayPoint>();


    public bool NoWayPoints() { return points.Count <= 0; }
    public bool HasOnlyOnePoint() { return points.Count == 1; }

    public WayPoint PathStart
    {
        get
        {
            if (points.Count <= 0) return null;
            return points[0];
        }
    }
    public WayPoint  PathSecond
    {
        get
        {
            if (points.Count <= 1) return null;
            return points[1];
        }
    }
    public WayPoint PathEnd
    {
        get
        {
            if (points.Count <= 0) return null;
            return points[points.Count-1];
        }
    }

    public bool AddPointToPath(WayPoint point) 
    {
        if (point == null) return false;
        if (NoWayPoints()) { 
            points.Add(point);
            return true; 
        }
        if (points.Contains(point)) return false;
        if (!PathEnd.IsNextPoint(ref point)) return false;
        points.Add(point);
        
        return true;
    }

    public bool TruncatePathTo(WayPoint point) {
        if (!point) return false;
        if (points.Count < 2) return false;
        int index = points.IndexOf(point);
        if (index < 0) return false;
        if( index==points.Count-1)return false;
        points.RemoveRange(index, points.Count-index);
        return true;
    }









    public void Reset()
    {
        points.Clear();
    }

    public bool AddAsNeighbor(WayPoint point)
    {
        if (!point || points == null || points.Count < 2) return false;
        if (point == PathEnd) return false;
        WayPoint parent = points[points.Count - 2];
        if (!parent.CanAccesTo(point)) return false;

        RemoveLast();
        points.Add(point);
        return true;

    }

    public void RemoveLast()
    {
        if (points == null || points.Count <= 0) return;
        points.RemoveAt(points.Count - 1);
    }

    public bool HasPoint()
    {
        return points.Count > 0;
    }

    public void RemoveFirst()
    {
        if (HasPoint())
            points.RemoveAt(0);
    }

    
}