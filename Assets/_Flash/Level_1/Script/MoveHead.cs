﻿using UnityEngine;
using System.Collections;

public class MoveHead : MonoBehaviour {

    public MapPathMaker pathMaker;
    public Transform head;
    public float nextToPoint = 0.1f;
    public float speed = 1f;
    public bool nextHasToBeValide = true;

    public WayPoint nextPoint;
    public WayPoint lastArrivedPoint;
    public bool freeze;

    public Animator playerAnimator;
    public AudioSource walkSound;
    public bool isWalking = false;
    private float _timeRan;

    public float TimeRan
    {
        get { return _timeRan; }
        private set { _timeRan = value; }
    }
    

    public void Start()
    {
        _timeRan = 0f;
    }

    public void OnEnable()
    {
        if(pathMaker)
            pathMaker.onPathChanged+=GoWhere;
    }

  
    public void OnDisable()
    {
        if (pathMaker)
            pathMaker.onPathChanged -= GoWhere;

    }
    private void GoWhere(ref Path path)
    {
        if((path==null || path.PathSecond==null)&& head!=null) 
        {nextPoint = WayPoint.GetCloser(head.transform.position);
            return;
        }
        if (nextPoint != path.PathSecond)
        {
            nextPoint = path.PathSecond;
            if (playerAnimator)
                playerAnimator.SetBool("IsWalking", nextPoint!=null);
        }
    }

    public void Update() 
    {

        if (freeze) return;
        if (!nextPoint) return;

        bool isArrived = GoAtPoint(nextPoint);
        if (isArrived) 
        {
            lastArrivedPoint = nextPoint;
            nextPoint = null;
            if(pathMaker.path.HasPoint())
            pathMaker.RemoveFirstWayPoint();
        }
    }


    private bool GoAtPoint(WayPoint nextWayPoint)
    {
        if (nextPoint == lastArrivedPoint)
        {
          

            return false;
        }

        isWalking = true;
       
        
        Vector3 dir = (nextWayPoint.transform.position - head.position).normalized;
        head.position += dir * (speed * Time.deltaTime);
        _timeRan += Time.deltaTime;
        if (Vector2.Distance(head.position, nextWayPoint.transform.position) < nextToPoint)
        {
            isWalking = false;
            return true;
        }
        return false;
    }

}
