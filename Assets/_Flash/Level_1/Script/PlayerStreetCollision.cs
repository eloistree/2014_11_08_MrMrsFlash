﻿using UnityEngine;
using System.Collections;

public class PlayerStreetCollision : MonoBehaviour {

    public string storeNameContain = "Store";
    public string dangerNameContain = "Danger";

    public Animator playerAnimator;
    public string isStun = "IsStun";
    public void OnTriggerEnter(Collider col) 
    {
        if (col.gameObject.name.Contains(storeNameContain))
        {
            Sound.Play("WinLvl1");
            CurrentLevelManager.NotifyLevelWin();
        }
        if (col.gameObject.name.Contains(dangerNameContain))
        {
            CurrentLevelManager.NotifyLevelLost();
            if (playerAnimator != null) playerAnimator.SetTrigger(isStun);
        }
    }
}
