﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomDangerZone : MonoBehaviour {


    public int dangerNumber = 2;

    public List<int> zones = new List<int> { 1, 2, 3, 4,5 };
    public List<WayPoint> waitPointUsed = new List<WayPoint>();

	void OnEnable () {

        int count = dangerNumber;
        int inifiLoopStop = 20;
        while (count > 0 && inifiLoopStop>0)
        {
            int randomZone = zones[Random.Range(0, zones.Count)];
            WayPoint wp = WayPoint.GetRandomInZone(randomZone);
            
            if (wp != null && ! waitPointUsed.Contains(wp))
            {

             Dangers.CreateDangerTo(wp.transform.position);
             zones.Remove(randomZone);
             waitPointUsed.Add(wp);
             count--;
         }
         inifiLoopStop--;
        }
	}
	
	
}
