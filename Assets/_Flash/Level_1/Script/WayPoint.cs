﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WayPoint : MonoBehaviour {

    public static List<WayPoint> wayPointsInScene = new List<WayPoint>();
    public List<WayPoint> accesTo;
    [Range(0,9)]
    public int[] belongToZone;



    void OnEnable()
    {
    
        wayPointsInScene.Remove(this);
        wayPointsInScene.Add(this);


    }

    void OnDisable() 
    {
        wayPointsInScene.Remove(this);
    }


    public bool IsNextPoint(ref WayPoint point)
    {
        if (!point) return false;
        return accesTo.Contains(point);
    }

    public static WayPoint GetCloser(Vector3 pos)
    {
        WayPoint closer = null;
        float distance = Mathf.Infinity;
        foreach (WayPoint wp in wayPointsInScene)
        { 
            if(wp){
            float dTmp =Vector3.Distance(pos,wp.transform.position);
            if (dTmp < distance) { closer = wp; distance = dTmp; }
            
            }
        }
        return closer;
    }

    public bool CanAccesTo(WayPoint point)
    {
        if (point == null) return false;
        return accesTo.Contains(point);
    }

    public static WayPoint GetRandom()
    {
        if(wayPointsInScene==null || wayPointsInScene.Count==0)return null;
        return wayPointsInScene[Random.Range(0, wayPointsInScene.Count)];
    }

    public static object ZoneCount { get; set; }

    public static List<WayPoint> GetWayPointsInZone(int index) {
        List<WayPoint> wps = new List<WayPoint>();

        foreach (WayPoint wp in wayPointsInScene)
            if (wp)
                foreach (int btz in wp.belongToZone)
                    if (btz == index)
                        wps.Add(wp);
        return wps;
    }



    public  static WayPoint GetRandomInZone(int index)
    {
        List<WayPoint> wps = GetWayPointsInZone(index);
        if (wps == null || wps.Count <= 0) return null;

        return wps[Random.Range(0, wps.Count)];


    }
}
