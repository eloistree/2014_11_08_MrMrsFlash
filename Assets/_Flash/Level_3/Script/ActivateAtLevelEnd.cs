﻿using UnityEngine;
using System.Collections;

public class ActivateAtLevelEnd : MonoBehaviour {

    public GameObject[] lose;
    public GameObject[] win;
    public GameObject[] timeout;

    private bool displayedOne;
    void OnEnable()
    {
        CurrentLevelManager.InstanceInScene.onLevelWin += ActivateOnWin;
        CurrentLevelManager.InstanceInScene.onLevelLost += ActivateOnLose;
        CurrentLevelManager.InstanceInScene.onTimeout += ActivateOnTimeout;
        //Debug.Log("Hello:" + CurrentLevelManager.InstanceInScene, CurrentLevelManager.InstanceInScene.gameObject);
    }
    void OnDisable()
    {
        //CurrentLevelManager.InstanceInScene.onLevelWin -= ActivateOnWin;
        //CurrentLevelManager.InstanceInScene.onLevelLost -= ActivateOnLose;
        //CurrentLevelManager.InstanceInScene.onTimeout -= ActivateOnTimeout;
    }

    private void ActivateOnTimeout()
    {
        if (displayedOne) return;
        ActivaAll(timeout, true);
    }

    private void ActivateOnWin(float timeLeft)
    {
        if (displayedOne) return;
        ActivaAll(win, true);
    }

    private void ActivateOnLose(float timeLeft)
    {
        if (displayedOne) return;
        ActivaAll(lose,true);
    }
    private void ActivaAll(GameObject[] objects, bool onOff) {
        displayedOne = true;
        foreach (GameObject gamo in objects)
            if (gamo) { gamo.SetActive(true); }
    }


   
	
	
}
