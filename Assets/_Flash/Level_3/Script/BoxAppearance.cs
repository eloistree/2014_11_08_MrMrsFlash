﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoxAppearance : MonoBehaviour {



    public static BoxAppearance InstanceInScene;

    public Sprite[] appearances;
    private List<Sprite> nextToCome = new List<Sprite>();

    void OnEnable()
    {
        InstanceInScene = this;
    }

    public static Sprite GetRandom()
    {
        if (!InstanceInScene) return null;
        return InstanceInScene.GetNext();
    }

    private void CheckComingList()
    {
        if (nextToCome.Count < 1)
            nextToCome.AddRange(appearances);
    }
    private Sprite GetNext()
    {
        if (nextToCome.Count < 1)
        { CheckComingList(); }
        int i = Random.Range(0, InstanceInScene.nextToCome.Count);
        Sprite g = nextToCome[i];
        nextToCome.RemoveAt(i);
        return g;

    }
}
