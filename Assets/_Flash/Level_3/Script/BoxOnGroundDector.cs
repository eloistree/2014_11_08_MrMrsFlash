﻿using UnityEngine;
using System.Collections;
using Flash.Level3;

public class BoxOnGroundDector : MonoBehaviour {


    public string boxName="Box";
    public bool goodBoxFallen;
    public bool badBoxFallen;

    void OnTriggerEnter2D(Collider2D col) 
    {

        if (!col.gameObject.name.Contains(boxName)) return;

        MovingBox box = col.GetComponent<MovingBox>() as MovingBox;
        if (!box) return;

        if (box.type == MovingBox.BoxType.Bad)
            badBoxFallen = true;
        else
        {
             goodBoxFallen = true;
        }
            
    }

    public bool HasBadBoxFallen() { return badBoxFallen; }
    public bool HasGoodBoxFallen() { return goodBoxFallen; }
}
