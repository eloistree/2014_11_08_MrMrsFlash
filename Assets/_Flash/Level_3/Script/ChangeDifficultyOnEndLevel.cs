﻿using UnityEngine;
using System.Collections;

public class ChangeDifficultyOnEndLevel : MonoBehaviour {

    public CreateBox_BaseOnDifficulty creator;

    void OnEnable()
    {
        CurrentLevelManager.InstanceInScene.onLevelWin += IncreaseDifficulty;
        CurrentLevelManager.InstanceInScene.onLevelLost += DecressDifficulty;
    }

    private void DecressDifficulty(float timeLeft)
    {
        if (!creator) return;
        creator.DecreaseDifficulty();
    }

    private void IncreaseDifficulty(float timeLeft)
    {
        if (!creator) return;
        creator.IncreaseDifficulty();
    }
    void OnDisable()
    {
        CurrentLevelManager.InstanceInScene.onLevelWin -= IncreaseDifficulty;
        CurrentLevelManager.InstanceInScene.onLevelLost -= DecressDifficulty;
    }

    
}
