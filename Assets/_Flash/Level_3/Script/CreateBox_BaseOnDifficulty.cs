﻿using UnityEngine;
using System.Collections;

public class CreateBox_BaseOnDifficulty : MonoBehaviour {

    public Transform where;
    public GameObject[] easy;
    public GameObject[] normal;
    public GameObject[] hard;

    public enum Difficulty { Easy, Normal, Hard }
    public static Difficulty difficulty = Difficulty.Easy;

    public delegate void OnObjectsCreated(GameObject root, Difficulty difficulty);
    public OnObjectsCreated onObjectsCreated;

    public void Start()
    {
        if (LevelDifficulty.InstanceInScene)
            difficulty = ConvertDifficulty( LevelDifficulty.InstanceInScene.CurrentDifficulty);
        CreateBoxesDependingOfTheDifficulty();
    }

    private Difficulty ConvertDifficulty(LevelDifficulty.Difficulty difficulty)
    {
        switch (difficulty)
        {
            case LevelDifficulty.Difficulty.Normal: return Difficulty.Normal;
            case LevelDifficulty.Difficulty.Hard: return Difficulty.Hard;
        }
        return Difficulty.Easy;
    }

    private void CreateBoxesDependingOfTheDifficulty()
    {

        GameObject[] boxes = GetPossibility(difficulty);
        if (boxes == null) return;
        if (boxes.Length < 1) return;
        GameObject toCreate = boxes[Random.Range(0, boxes.Length)];
        if (toCreate == null) return;
        GameObject gamo =GameObject.Instantiate(toCreate) as GameObject;
        if (where) gamo.transform.parent = where;
        if (onObjectsCreated != null)
            onObjectsCreated(gamo, difficulty);
    }

    private GameObject[] GetPossibility(Difficulty difficulty)
    {
        switch (difficulty)
        {
            case Difficulty.Easy: return easy;
            case Difficulty.Normal: return normal;
            case Difficulty.Hard: return hard;
        }
        return normal;
    }

    public void SetDifficulty(Difficulty newDifficulty) 
    {
        difficulty = newDifficulty;
        Debug.Log("Diff:" + newDifficulty);
    }

    public void IncreaseDifficulty()
    {
        switch (difficulty)
        {
            case Difficulty.Easy: difficulty = Difficulty.Normal; break;
            case Difficulty.Normal: difficulty = Difficulty.Hard; break;
        }
           //Debug.Log("Inc" + difficulty);
    }

    public void DecreaseDifficulty()
    {
        switch (difficulty)
        {
            case Difficulty.Normal: difficulty = Difficulty.Easy; break;
            case Difficulty.Hard: difficulty = Difficulty.Normal; break;
        }
        //Debug.Log("Dec" + difficulty);
    }

}
