﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 namespace Flash.Level3 {
public class MovingBox : MonoBehaviour {

    public enum BoxType { Bad,Good }
    public BoxType type;

    private static int moveBoxTry;

    public static int TryToMoveBox
    {
        get { return moveBoxTry; }
        private set { moveBoxTry = value; }
    }
    

    public float maxTimeDrag = 0.8f;
    private float maxDragCountDown = 0.8f;
    private bool isDragging;
    private Vector3 startDragMousePosition;
    public float moveForcePerSecond=1f;
    public float forceGiven = 0f;
    public float maxForceGiven = 10f;
    public float maxMagnitude = 60f;

    public TrackedPoint trackedPoint;
    public float movingSensibility=0.05f;

    public static List<MovingBox> boxInScene = new List<MovingBox>();

    public float removeVelocityWhenUp=0.9f;

    public bool movementAllow=true;

    private bool isInGoodHand;

    public bool IsInGoodHand
    {
        get { return isInGoodHand; }
        set { isInGoodHand = value; }
    }
    
    private void Start() {
        moveBoxTry = 0;
    }

    public ForceMode2D forceMode;
    public void SetMovementAllow(bool onOff) 
    {
        movementAllow = onOff;
    }

    void OnEnable()
    {
        if (!trackedPoint) trackedPoint = GetComponent<TrackedPoint>() as TrackedPoint;
        boxInScene.Add(this);
    }
    void OnDisable()
    {
        boxInScene.Remove(this);
    }
    

    public bool IsMoving() 
    {
        if (trackedPoint==null) return false;
        return Mathf.Abs(trackedPoint.Speed) >= movingSensibility;
    }
	

	void Update () {

        if (!isDragging) return;
        if (maxForceGiven < forceGiven) return;
        maxDragCountDown -= Time.deltaTime;
        if (maxDragCountDown < 0f)
        {
            maxDragCountDown = 0;
            isDragging = false;
        }
        Vector3 dir =  (Input.mousePosition-startDragMousePosition);
        if (dir.magnitude > maxMagnitude)
            dir = dir.normalized * maxMagnitude
; if (movementAllow)
        {
            this.rigidbody2D.AddForce(dir * (Time.deltaTime * moveForcePerSecond), forceMode);
            forceGiven += Time.deltaTime * moveForcePerSecond;
        }
	}
    void OnMouseDown()
    {
        //Debug.Log("" + Input.mousePosition + " : " + transform.position);
        //Debug.Log("In", this);
        isDragging = true;
        forceGiven = 0f;
        startDragMousePosition = Input.mousePosition;
        maxDragCountDown = maxTimeDrag;
        Sound.Play("boxMovingStart");
    }
    void OnMouseUp()
    {
        //Debug.Log("Out", this);
        isDragging = false;
        startDragMousePosition = Vector3.zero;

            rigidbody2D.velocity = rigidbody2D.velocity * removeVelocityWhenUp;
            moveBoxTry++;
        ////////////////////////////D
    }
    
    void OnMouseDrag()
    {
        if (!isDragging) return;
        //Debug.Log("Dir: " + (Input.mousePosition - startDragMousePosition), this);

    }

    public static bool IsBadBoxMoving()
    {
    
        bool isMoving=false;
        foreach (MovingBox box in boxInScene) 
        {
            if (box && box.type == MovingBox.BoxType.Bad && box.IsMoving()) {
                isMoving = true;
            }
        }
        
        return isMoving;

    }
}
    }