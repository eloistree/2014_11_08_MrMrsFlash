﻿using UnityEngine;
using System.Collections;
using Flash.Level3;

[RequireComponent(typeof(MovingBox))]
public class OnTimeEndRemoveBoxInput : MonoBehaviour {


    public void OnEnable()
    {

        LevelTimer.onTimerEnd += DisableInput;
    }
    public void OnDisable()
    {

        LevelTimer.onTimerEnd -= DisableInput;
    }

    private void DisableInput(float definedTime)
    {
        MovingBox box = GetComponent<MovingBox>() as MovingBox;
        if (!box) return;
        box.SetMovementAllow(false);
    }
}
