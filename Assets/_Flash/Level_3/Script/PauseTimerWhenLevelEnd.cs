﻿using UnityEngine;
using System.Collections;

public class PauseTimerWhenLevelEnd : MonoBehaviour {

	 void OnEnable()
    {

        CurrentLevelManager.InstanceInScene.onLevelEnd += PauseTheTimer;
    }

    
    void OnDisable()
    {
        CurrentLevelManager.InstanceInScene.onLevelEnd -= PauseTheTimer;

    }
    public void PauseTheTimer(float timeLeft)
    {

        if (LevelTimer.InstanceInScene)
            LevelTimer.InstanceInScene.SetPause(true);
    }
}
