﻿using UnityEngine;
using System.Collections;

public class RandomAppearanceBox : MonoBehaviour {

    void Start()
    {
        if (BoxAppearance.InstanceInScene == null) return;
        Sprite spriteRandom = BoxAppearance.GetRandom();
        SpriteRenderer sr = GetComponent<SpriteRenderer>() as SpriteRenderer;
        sr.sprite = spriteRandom;
	}
	
}
