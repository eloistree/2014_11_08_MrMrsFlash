﻿using UnityEngine;
using System.Collections;
using System;

public class RestartLevelOnEndDetected : MonoBehaviour {


    public float custumTimeToLoad=-1f;
    public string nextLevelName="";
    public bool restartIfNotFound = false;

    void OnEnable()
    {
        CurrentLevelManager.InstanceInScene.onLevelEnd += Load;
       
    }

    void OnDisable()
    {
        CurrentLevelManager.InstanceInScene.onLevelEnd -= Load;
    }

    private void Load(float timeLeft)
    {
       Invoke( "Load",0.1f);
    }
    

    private void Load()
    {
        
        bool loadSuccess = custumTimeToLoad>=0?LevelGuide.LoadNextLevel( custumTimeToLoad,0f):LevelGuide.LoadNextLevel();
        if (!loadSuccess && restartIfNotFound)
        {
            Debug.LogWarning("No next Level -> Restart");
            Application.LoadLevel(Application.loadedLevel);
        }
    }
}
