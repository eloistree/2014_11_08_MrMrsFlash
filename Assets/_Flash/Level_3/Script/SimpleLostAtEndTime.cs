﻿using UnityEngine;
using System.Collections;

public class SimpleLostAtEndTime : MonoBehaviour {


	void Start () {
        LevelTimer.onTimerEnd += LostTimeOut;
	}

    private void LostTimeOut(float definedTime)
    {
        CurrentLevelManager.NotifyLevelLost();
    }

	
}
