﻿using UnityEngine;
using System.Collections;

public class TrackedPoint : MonoBehaviour, ITrackedPoint
{
    #pragma warning disable 0618
    public string idName = "Unnamed";
    public Transform point;
    private Vector3 _direction;

    public Vector3 Direction
    {
        get { return _direction.normalized; }
       private set { _direction = value; }
    }
   public float _speed;

    public float Speed
    {
        get { return _speed; }
        private set
        {
            _speed = value;
        }
    }

    

    public Vector3 Position
    {
        get { return point.position; }
    }
    public Quaternion Rotation
    {
        get { return point.rotation; }
    }
    


	// Use this for initialization
	void Start () {
        if (point == null) {
            point = transform;
           // Debug.LogWarning("A tracked point is not assigned",this.gameObject); Destroy(this); return;
        }

    
    }

   // private Quaternion lastRotation;
    private Vector3 lastPosition;

	void Update () {
        if (point == null) return;
        float dt = Time.deltaTime;

        Direction = point.position - lastPosition;
        Speed = Vector3.Distance(point.position , lastPosition) / dt;
        


        lastPosition = point.position;
        //lastRotation = point.rotation;

	}


    public Vector3 NextPoint(float time)
    {
        return Position + Direction * Speed*time;
    }
}
