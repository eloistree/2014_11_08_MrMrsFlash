﻿using UnityEngine;
using System.Collections;
using Flash.Level3;

public class WinIfBoxOkAndStabilised : MonoBehaviour {

    public bool stopChecking;
    public BoxOnGroundDector[] boxGroundDector;
    private MovingBox goodBox;
    public float refresh = 0.1f;
    public float lastMovingTime;
    public float timeNotMovingBadBox=2f;
    private bool soundPlayed;

    void Start() {
        boxGroundDector = GameObject.FindObjectsOfType<BoxOnGroundDector>() as BoxOnGroundDector[];
        StartCoroutine(CheckVictory());

        CheckForGoodBox();

    }
    public void OnLevelWasLoaded(int i)
    {
        soundPlayed = false;
     
    }

    private void CheckForGoodBox()
    {
        if (goodBox == null) {

            MovingBox[] boxes = GameObject.FindObjectsOfType<MovingBox> () as MovingBox[];
            foreach (MovingBox b in boxes)
                if (b != null && b.type == MovingBox.BoxType.Good)
                {
                    goodBox = b; return;
                }
        }

    }

    void OnEnable()
    {
        LevelTimer.onTimerEnd += OnTimeout;
    }
    void OnDisable()
    {
        LevelTimer.onTimerEnd -= OnTimeout;
    }

    private void OnTimeout(float definedTime)
    {
        if (!GoodBoxFallen())
            CurrentLevelManager.NotifyLevelTimeout();
    }


    public IEnumerator CheckVictory() 
    {
        
        while (!stopChecking)
        {
            if (MovingBox.IsBadBoxMoving())
            {
                lastMovingTime = Time.timeSinceLevelLoad;
            }

            if (GoodBoxFallen()) 
            {

                if ((Time.timeSinceLevelLoad - lastMovingTime) > timeNotMovingBadBox)
                {
                    if (!soundPlayed) { 
                    Sound.Play("WinLvl3");
                    soundPlayed = true;
                    }
                    CurrentLevelManager.NotifyLevelWin();
                }
            }
            if (BadBoxFallen())
            { 
                CurrentLevelManager.NotifyLevelLost(); 
            }
            yield return new WaitForSeconds(refresh) ;
        }

    
    }

    private bool GoodBoxFallen()
    {
        CheckForGoodBox();
        if (goodBox == null) return false;
        return goodBox.IsInGoodHand;
    }
    private bool BadBoxFallen()
    {
        foreach (BoxOnGroundDector groundDetector in boxGroundDector)
        {
            if (groundDetector && groundDetector.HasBadBoxFallen())
                return true;
        }
        return false;
    }
   

}
