﻿using UnityEngine;
using System.Collections;

public class Couverture : MonoBehaviour {
	bool preselection;

    [HideInInspector]
    public string Scene;
    public float MyAngle;

	void Update () {
        //Garde les couvertures face à la caméra.
		Vector3 toCamera = transform.position - Camera.main.transform.position;
		toCamera.y = 0;
		transform.rotation = Quaternion.LookRotation(toCamera, -Vector3.up);
	}

	void OnMouseDown()
	{
		//Si il est déjà préséléctionné, exécute le LoadLevel correspondant.
		if (preselection)
		{
			if(Scene != string.Empty)Application.LoadLevel(Scene);
		}
		else
		{   //Broadcast l'exécution de ClearSelection à tout les autres Childs.
            //Préséléctionne la couverture lors du premier "clic" sur lui.
			transform.parent.BroadcastMessage("ClearSelection");
			preselection = true;
            GameObject.Find("Sphere").GetComponent<Tourniquet>().Focus(MyAngle);
		}
	}

	void ClearSelection()
	{
		preselection = false;
	}
}