﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class CouverturesManager {
	public GameObject Couverture;
    public Texture Image;
    public string SceneALoad;
    public GameObject prefabLevelGuide;
}