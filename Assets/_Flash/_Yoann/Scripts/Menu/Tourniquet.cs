﻿using UnityEngine;
using System.Collections;

public class Tourniquet : MonoBehaviour {

	// Tourniquet/CouverturesManager/Couverture

	float diametre;
	int nbCouv; //Le nombre de quartier d'angles qu'il y aura pour les placer.
	float angleSeparator; //Angle entre chaque couverture (360°/nbCouv).
	float angleCurrent;
	int count=0; //Pour le placement.
	GameObject Y;
    float posZAlpha;
    public float AlphaRatio = 0.3f; //l'alpha qu'aura le plus éloigné sur le tourniquet. C'est un ratio de 0.0f à 1.0f
	public float TurnSpeed = 0.4f;
	Vector3 centerPos, targetPos, refPos;
    GameObject[] saveGO;

	void Start () {
        TurnSpeed = TurnSpeed*2 / (Screen.width / 1024f);
        diametre = gameObject.transform.localScale.x;
        nbCouv = gameObject.GetComponentInChildren<ObjectCouverture>().Pack.Length;
		angleSeparator = 360 / nbCouv;
        saveGO = new GameObject[20];

        foreach (CouverturesManager x in gameObject.GetComponentInChildren<ObjectCouverture>().Pack) //Place les couvertures de façon homogène sur la circonférence de la sphère.
		{
            //Création du material et ajout de la texture appliqués sur le GameObject Couverture.
            x.Couverture.renderer.material = new Material(Resources.Load<Shader>("Alpha-Diffuse"));
            x.Couverture.renderer.material.mainTexture = x.Image;
            x.Couverture.GetComponent<Couverture>().Scene = x.SceneALoad;
            

            angleCurrent = angleSeparator * count * Mathf.Deg2Rad;
			Y = (GameObject)GameObject.Instantiate(x.Couverture,Vector3.zero,Quaternion.identity);
            Y.transform.SetParent(transform.GetChild(0).transform);
			Y.transform.position = new Vector3(Mathf.Cos(angleCurrent) * diametre/2 , Y.transform.parent.transform.position.y, Mathf.Sin(angleCurrent) * diametre/2 );
            Y.GetComponent<Couverture>().MyAngle = (angleSeparator * count) + 90;
          
            saveGO[count] = Y;
            count = count+1;
			Y = null;
		}
		count = 0;
	}

    public void Focus(float angle)
    {
        //transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        transform.eulerAngles = new Vector3(0, angle, 0);
    }

	void Update()
	{
		//Déplacement: Tourner
		if (Input.GetMouseButtonDown(0))
		{
			centerPos = Input.mousePosition;
		}
     
		if (Input.GetMouseButton(0))
		{
			targetPos = Input.mousePosition;
		}

		centerPos = Vector3.SmoothDamp(centerPos, targetPos, ref refPos, 0.5f); //Lissage du mouvement avec rémanance du mouvement ("lerp")
		Vector3 targetRotate = new Vector3(0, -(targetPos.x - centerPos.x), targetPos.z - centerPos.z);
        transform.Rotate(targetRotate * Time.deltaTime * TurnSpeed, Space.World); // Rotation de la sphere masquée

        //debug
            //Debug.Log(saveGO[0].transform.position.x);

        foreach(GameObject x in saveGO)
        {
            if (x != null)
            {
                posZAlpha = (x.transform.position.z + (GameObject.Find("Sphere").transform.localScale.x/2)) / GameObject.Find("Sphere").transform.localScale.x;
                //Debug.Log(GameObject.Find("Sphere").transform.localScale.x - posZAlpha);
                x.renderer.material.SetColor("_Color", new Vector4(x.renderer.material.color.r, x.renderer.material.color.g, x.renderer.material.color.b, 1f + AlphaRatio - posZAlpha));
            }
        }
	}
}