﻿using UnityEngine;
using System.Collections;

public class ViewPort : MonoBehaviour {

    void Start () {
        float xFactor = Screen.width / 1024f;
        float yFactor = Screen.height / 768f;
        GUIUtility.ScaleAroundPivot(new Vector2(xFactor, yFactor), Vector2.zero);
    }
	
}