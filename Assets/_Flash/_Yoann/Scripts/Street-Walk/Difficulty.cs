﻿using UnityEngine;
using System.Collections;

public class Difficulty : MonoBehaviour {

    public enum Difficulte { Easy, Normal, Hard, First }
    public static Difficulte difficulty; //retiré le static pour l'avoir dans l'Inspector, le temps des tests.
    public GameObject[] trous;
    public GameObject[] taques;
	public int NbTrou = 0;
 
    void Awake()
    {
		if (LevelDifficulty.InstanceInScene != null)
						SetDifficulty((Difficulte)LevelDifficulty.InstanceInScene.CurrentDifficulty);
				else
						SetDifficulty(3);
        
		LevelTimer.defaultTime = GameObject.Find("Character").GetComponent<MoveForward>().timeOfMove;
        ApplyDifficulty(difficulty);
    }

    public void SetDifficulty(Difficulte diff)
    {
        difficulty = diff;
    }

    public void SetDifficulty(int diff)
    {
        switch (diff)
        {
            case 1: difficulty = Difficulte.Easy; break;
            case 2: difficulty = Difficulte.Normal; break;
            case 3: difficulty = Difficulte.Hard; break;
			case -1: difficulty = Difficulte.First; break;
            default: difficulty = Difficulte.Normal; break;
        }
    }

    private int reverseDifficulteToInt(Difficulte diff)
    {
        switch (diff)
        {
            case Difficulte.Easy: return 1;
            case Difficulte.Normal: return 2;
            case Difficulte.Hard: return 3;
			case Difficulte.First: return -1;
            default: return 2;
        }
    }

    private void ApplyDifficulty(Difficulte diff)
	{	
        int index = reverseDifficulteToInt(diff);
		int z = Random.Range(0,4);
            if(index == 3)
            {
				//while(trous[z].activeInHierarchy)
				//{
				//	z = Random.Range(0,4);
				//}
				trous[3].SetActive(true);
                taques[3].SetActive(true);

				//while(trous[z].activeInHierarchy) 
				//{
				//	z=Random.Range(0,4);
				//}
				trous[2].SetActive(true);
                taques[2].SetActive(true);
				NbTrou = NbTrou+2;
            }

            if(index >= 2)
             {
				while(trous[z].activeInHierarchy || trous[z].name == "trou4")
				{
					z=Random.Range(0,3);
				}
				trous[z].SetActive(true);
                taques[1].SetActive(true);
				NbTrou++;
             }

		if(index >=1 || index == -1)
             {
				while(trous[z].activeInHierarchy || trous[z].name == "trou4")
				{
					z=Random.Range(0,3);
				}
				trous[z].SetActive(true);
                taques[0].SetActive(true);
				NbTrou++;
             }
        
    }
}