﻿using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour
{
    public float offsetTimeSplash = 1.5f;
    public GameObject SplashSpriteAnim;
    public bool soundActif;
	public static GameObject Run;
	bool played;
    public float timeScaleAtStart;


	void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			if (other.GetComponent<MoveForward>().isMoving)
			{
				other.GetComponent<AudioSource>().Stop();
				other.rigidbody2D.isKinematic = false;
				iTween.Stop(other.gameObject);
                GameObject.Find("masque1").GetComponent<SpriteRenderer>().sortingOrder = 11;
				GameObject.Find("masque2").GetComponent<SpriteRenderer>().sortingOrder = 11;
				other.GetComponent<Animator>().SetBool("isFalling", true);
				PlayerPrefs.SetInt("Flash:"+gameObject.name,1);
				if(PlayerPrefs.GetInt("Flash:trou1")==1 && PlayerPrefs.GetInt("Flash:trou2")==1 && PlayerPrefs.GetInt("Flash:trou3")==1 && PlayerPrefs.GetInt("Flash:trou4")==1)
				{
					Achievements.SetAchievementUnlockState("mixer_level2_egout", true);
				}
				if(!played) StartCoroutine("Splash");
                
				Camera.main.GetComponent<camera>().Stop();
                GameObject taque;
                for ( int i = 1; i <= 4 ; i++ )
                {
                    taque = GameObject.Find("taque" + i);
                    if (taque != null)
                    {
                        taque.GetComponent<dragging>().isDraggable = false;
                        taque.GetComponent<Collider2D>().enabled = false;
                    }
                }
			}
		}
		if (other.tag == "Taque")
		{
            dragging dragg = other.GetComponent<dragging>();
            if (dragg && !dragg.isDragging)
			{
				other.transform.position = transform.position;
				Sound.Play("Taque",null,true,Sound.SoundPickUpType.Randomly,4,50,0,100);
                dragg.isDraggable = false;
                taque taque = other.GetComponent<taque>();
                taque.locked = true;
                taque.turnOffHighlight();
                SpriteRenderer sr = other.GetComponent<SpriteRenderer>() as SpriteRenderer;
                taque.ChangeSortingOrder(sr, 5);
				gameObject.collider2D.enabled = false;
				GameObject.Find ("Character").GetComponent<MoveForward>().nbTrouBouche++;
				if(GameObject.Find ("Character").GetComponent<MoveForward>().nbTrouBouche == GameObject.Find ("GameDifficulty").GetComponent<Difficulty>().NbTrou)
				{
					Destroy (MoveForward.Footsteps);
					Run = Sound.Play("Run");
					Time.timeScale = 3;
				}
			}
		}
	}

    public IEnumerator Splash()
    {
		played = true;
		Destroy (MoveForward.Footsteps);
		LevelTimer.InstanceInScene.pause = true;
		Sound.Play ("Splash",null,true,Sound.SoundPickUpType.Randomly,10,100,0f,100);
		yield return new WaitForSeconds(offsetTimeSplash);
        SplashSpriteAnim.SetActive(true);
        if(soundActif)
		Sound.Play ("Lose",null,true,Sound.SoundPickUpType.Randomly,10,100,0.5f,100);
		yield return new WaitForSeconds (2);
		CurrentLevelManager.NotifyLevelLost();
    }
    public void Start()
    {
        timeScaleAtStart = Time.timeScale;
    }

    public void OnDisable()
    {
        Time.timeScale = timeScaleAtStart;
    }
}