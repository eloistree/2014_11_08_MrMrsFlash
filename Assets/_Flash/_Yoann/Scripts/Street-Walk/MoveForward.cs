﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {

	public float timeOfMove;
    public GameObject Arrivee;
	public AudioClip Win;
	[HideInInspector]
	public bool isMoving;
	public int nbTrouBouche=0;
	public static GameObject Footsteps;
	
	void Start () {
		isMoving = true;
		iTween.MoveTo(gameObject, iTween.Hash("x", Arrivee.transform.position.x, "easeType", "linear", "time", timeOfMove, "onComplete", "Switch"));
		StartCoroutine ("Feet");
	}

	IEnumerator switchMoveStat()
	{
		gameObject.rigidbody2D.isKinematic = true;
		GetComponent<Animator>().SetBool("isArrived", true);
		Destroy (Hole.Run);
		Sound.Play ("Win");
		//Time.timeScale = 1;
		yield return new WaitForSeconds (1);
		CurrentLevelManager.NotifyLevelWin();
	}

	void Switch()
	{
		StartCoroutine ("switchMoveStat");
	}

	IEnumerator Feet()
	{
		yield return new WaitForSeconds(0.001f);
		Footsteps = Sound.Play ("Footsteps", null, true, Sound.SoundPickUpType.Randomly, timeOfMove, 40, 0f, 100);
	}
}
