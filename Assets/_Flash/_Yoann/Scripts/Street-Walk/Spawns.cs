﻿using UnityEngine;
using System.Collections;

public class Spawns : MonoBehaviour {
	public int childcount;
	int count = 1;
	void Awake()
	{
		childcount = gameObject.transform.childCount;
		foreach(Transform child in transform)
		{
			child.gameObject.name = child.gameObject.name + count.ToString();
            count++;
		}
	}
}