﻿using UnityEngine;
using System.Collections;

public class camera : MonoBehaviour {
    float xMax;
    float Ratio;
    public GameObject Destination;
    bool canTranslate;
	// Use this for initialization
	void Awake () {
        xMax = 52f;
        Ratio = Vector3.Distance(transform.position, Destination.transform.position);
        canTranslate = true;
	}

    public void Stop()
    {
        canTranslate = false;
    }
	
	// Update is called once per frame
	void Update () {
	    if(transform.position.x <= xMax && canTranslate)
        {
            transform.Translate(Vector3.right * Time.deltaTime * Ratio /15);
        }
	}
}