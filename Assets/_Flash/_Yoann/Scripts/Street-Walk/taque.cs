﻿using UnityEngine;
using System.Collections;
using HighlightingSystem;

public class taque : MonoBehaviour {
	// Use this for initialization
	float CharY;
    bool placed;
    public bool locked;
	protected Highlighter h;
	public bool turnOnHighlight;
	public float flashFreq = 0.5f;
	public Color flashFirstColor = Color.green;
	public Color flashSecondColor = Color.clear;
	int conditionsortie = 0;

    public int fromLayerOrder = 8;
    public int toLayerOrder = 13;

    public delegate void OnSortingChange(SpriteRenderer renderer, int sortLayerNumber);
    public OnSortingChange onSortingChange;
    public delegate void OnTaqueFixed();
    public OnTaqueFixed onTaqueFixed;

	void Awake()
	{
		h = GetComponent<Highlighter> ();
        

	}

	void Start () {
		if (turnOnHighlight) h.FlashingOn (flashFirstColor, flashSecondColor, flashFreq);

        while(!placed && conditionsortie<100)
        {
            int rand = Random.Range(1, GameObject.Find("Spawns").GetComponent<Spawns>().childcount);
            if (!GameObject.Find(string.Format("spawn" + rand)).GetComponent<spawn>().oqp)
            {
                transform.position = GameObject.Find(string.Format("spawn" + rand)).transform.position;
                GameObject.Find(string.Format("spawn" + rand)).GetComponent<spawn>().oqp = true;
                placed = true;
            }
			conditionsortie++;
        }
        CharY = GameObject.Find("Character").transform.position.y;
	}
	
	// Update is called once per frame
    private int lastSortingOrder;
	void Update () {

		if(GetComponent<dragging>().isDraggable)
		{
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            if (sr)
            {
                if (transform.position.y > CharY + 2.2)
                {
                    ChangeSortingOrder(sr, fromLayerOrder);
                }
                else
                {
                    ChangeSortingOrder(sr, toLayerOrder);
                }
            }

		}
	}

    public void ChangeSortingOrder(SpriteRenderer sr,int sortOrder)
    {

        sr.sortingOrder = sortOrder;
        if (onSortingChange != null && sr.sortingOrder != lastSortingOrder)
            onSortingChange(sr, sr.sortingOrder);
        lastSortingOrder = sr.sortingOrder;
    }

	public void turnOffHighlight()
	{
				h.FlashingOff ();
                if (onTaqueFixed != null)
                {
                    onTaqueFixed();
                }
	}
}
